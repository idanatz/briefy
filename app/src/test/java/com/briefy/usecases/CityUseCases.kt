package com.briefy.usecases

import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.koin.test.KoinTest

/**
 * Created by Idan Atsmon on 04/07/2018.
 */

@RunWith(JUnit4::class)
class CityUseCases : KoinTest {

//    @Before
//    fun before() {
//        loadKoinModules(listOf(
//            applicationContext {
//                bean {
//                    Mockito.mock(RetrofitClient::class.java)
//                }
//            }
//        ))
//    }
//
//    @Test
//    fun testGetCityUseCase_Success() {
//        val retrofitClientMock : RetrofitClient by inject()
//
//        val goodCityModel = ModelGenerator.CityGenerator.createGoodCityModel()
//        val retrofitImplClassToMock = CityProviderUseCases.GetCity.RetrofitImpl::class.java
//        val retrofitImpl = Mockito.mock(retrofitImplClassToMock).also { Mockito.`when`(it.execute("Tel Aviv-Yafo")).thenReturn(Single.just(goodCityModel)) }
//        retrofitClientMock.also { Mockito.`when`(it.create(retrofitImplClassToMock)).thenReturn(retrofitImpl) }
//
//        CityProviderUseCases.GetCity("Tel Aviv-Yafo")
//                .execute()
//                .test()
//                .assertValue { OWWeatherResponse ->
//                    OWWeatherResponse is com.briefy.model.State.Result.Data<OWWeather> && OWWeatherResponse.data.name == "Tel Aviv-Yafo"
//                }
//                .assertComplete()
//    }
//
//    @Test
//    fun testGetCityUseCase_CityNotFound() {
//        val retrofitClientMock : RetrofitClient by inject()
//
//        val cityNotFoundModel = ModelGenerator.CityGenerator.createCityNotFoundOWWeatherModel()
//        val retrofitImplClassToMock = CityProviderUseCases.GetCity.RetrofitImpl::class.java
//        val retrofitImpl = Mockito.mock(retrofitImplClassToMock).also { Mockito.`when`(it.execute("Tel Aviv-Yafo")).thenReturn(Single.just(cityNotFoundModel)) }
//        retrofitClientMock.also { Mockito.`when`(it.create(retrofitImplClassToMock)).thenReturn(retrofitImpl) }
//
//        CityProviderUseCases.GetCity("Tel Aviv-Yafo")
//                .execute()
//                .test()
//                .assertValue { OWWeatherResponse ->
//                    OWWeatherResponse is com.briefy.model.State.Result.Error && OWWeatherResponse.errorType is Error.ErrorType.DataNotFound
//                }
//                .assertComplete()
//    }
}