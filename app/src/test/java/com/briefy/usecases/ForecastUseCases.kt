package com.briefy.usecases

import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.koin.test.KoinTest

/**
 * Created by Idan Atsmon on 04/07/2018.
 */

@RunWith(JUnit4::class)
class ForecastUseCases : KoinTest {

//    @Before
//    fun before() {
//        loadKoinModules(listOf(
//            applicationContext {
//                bean {
//                    Mockito.mock(RetrofitClient::class.java)
//                }
//            }
//        ))
//    }
//
//    @Test
//    fun testGetCurrentForecastUseCase_Success() {
//        val retrofitClientMock : RetrofitClient by inject()
//
//        val retrofitImplClassToMock = ForecastProviderUseCases.GetCurrentForecast.RetrofitImpl::class.java
//        val goodSingleForecastModel = ModelGenerator.ForecastGenerator.createGoodSingleForecastModel()
//        val retrofitImpl = Mockito.mock(retrofitImplClassToMock).also { Mockito.`when`(it.execute("Tel Aviv-Yafo")).thenReturn(Single.just(goodSingleForecastModel)) }
//        retrofitClientMock.also { Mockito.`when`(it.create(retrofitImplClassToMock)).thenReturn(retrofitImpl) }
//
//        ForecastProviderUseCases.GetCurrentForecast("Tel Aviv-Yafo")
//                .execute()
//                .test()
//                .assertValue { OWWeatherResponse ->
//                    OWWeatherResponse is State.Data<OWWeather> && OWWeatherResponse.data.name == "Tel Aviv-Yafo"
//                }
//                .assertComplete()
//    }
//
//    @Test
//    fun testGetThreeDaysForecastUseCase_Success() {
//        val retrofitClientMock : RetrofitClient by inject()
//
//        val retrofitImplClassToMock = ForecastProviderUseCases.GetThreeDaysForecast.RetrofitImpl::class.java
//        val goodMultipleForecastModel = ModelGenerator.ForecastGenerator.createGoodMultipleForecastsModel()
//        val retrofitImpl = Mockito.mock(retrofitImplClassToMock).also { Mockito.`when`(it.execute("Tel Aviv-Yafo", 24)).thenReturn(Single.just(goodMultipleForecastModel)) }
//        retrofitClientMock.also { Mockito.`when`(it.create(retrofitImplClassToMock)).thenReturn(retrofitImpl) }
//
//        ForecastProviderUseCases.GetThreeDaysForecast("Tel Aviv-Yafo", true)
//                .execute()
//                .test()
//                .assertValue { OWForecastResponse ->
//                    OWForecastResponse is State.Data<OWForecast> && OWForecastResponse.data.city?.name == "Tel Aviv-Yafo"
//                }
//                .assertComplete()
//    }
}