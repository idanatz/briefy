package com.briefy

import android.app.Application
import android.content.res.Resources
import com.briefy.di.*
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.koin.android.ext.koin.with
import org.koin.standalone.StandAloneContext.startKoin
import org.koin.test.KoinTest
import org.koin.test.dryRun
import org.mockito.Mockito
import org.mockito.Mockito.mock

/**
 * Created by Idan Atsmon on 04/07/2018.
 */

@RunWith(JUnit4::class)
class DependencyInjection : KoinTest {

//    @Test
//    fun testDryRun() {
//
//        val applicationMock = mock(Application::class.java)
//        val resourceMock = mock(Resources::class.java)
//        Mockito.`when`(applicationMock.resources).thenReturn(resourceMock)
//
//        // start Koin
//        startKoin(listOf(
//                localRepositoryModule,
//                phoneUtilitiesModule,
//                networkModule,
//                persistenceModule,
//                remoteDataProviderModule,
//                viewModule)) with applicationMock
//
//        // dry run of given module list
//        dryRun()
//    }
}