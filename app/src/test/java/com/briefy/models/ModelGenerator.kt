package com.briefy.models

import com.briefy.model.open_weather.ForecastDto
import com.briefy.model.open_weather.WeatherDto
import com.google.gson.Gson
import com.google.gson.stream.JsonReader
import java.io.File
import java.io.FileReader

/**
 * Created by Idan Atsmon on 16/07/2018.
 */

class ModelGenerator {

    companion object {
        private const val goodWeatherModelPath = "src/test/java/com/briefy/models/GoodWeatherOWModel.json"
        private const val goodForecastModelPath = "src/test/java/com/briefy/models/GoodForecastOWModel.json"
        private const val cityNotFoundModelPath = "src/test/java/com/briefy/models/CityNotFoundOWModel.json.json"
    }

    class CityGenerator {
        companion object {
            fun createGoodCityModel(): WeatherDto = Gson().fromJson(JsonReader(FileReader(File(goodWeatherModelPath))), WeatherDto::class.java)
            fun createCityNotFoundOWWeatherModel(): WeatherDto = Gson().fromJson(JsonReader(FileReader(File(cityNotFoundModelPath))), WeatherDto::class.java)
        }
    }

    class ForecastGenerator {
        companion object {
            fun createGoodSingleForecastModel(): WeatherDto = Gson().fromJson(JsonReader(FileReader(File(goodWeatherModelPath))), WeatherDto::class.java)
            fun createGoodMultipleForecastsModel(): ForecastDto = Gson().fromJson(JsonReader(FileReader(File(goodForecastModelPath))), ForecastDto::class.java)
        }
    }
}