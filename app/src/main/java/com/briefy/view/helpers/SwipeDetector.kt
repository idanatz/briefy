package com.briefy.view.helpers

import android.view.MotionEvent

/**
 * Created by Idan Atsmon on 20/03/2018.
 */

class SwipeDetector(private val swipingThreshold: Int = DEFAULT_SWIPING_THRESHOLD,
                    private val swipedUpDownThreshold: Int = DEFAULT_SWIPED_UP_DOWN_THRESHOLD,
                    private val swipedLeftRightThreshold: Int = DEFAULT_SWIPED_LEFT_RIGHT_THRESHOLD) {

    companion object {
        /**
         * Swiping threshold is added for neglecting swiping
         * when differences between changed x or y coordinates are too small
         */
        const val DEFAULT_SWIPING_THRESHOLD = 20
        /**
         * Swiped threshold is added for neglecting swiping
         * when differences between changed x or y coordinates are too small
         */
        const val DEFAULT_SWIPED_UP_DOWN_THRESHOLD = 100
        const val DEFAULT_SWIPED_LEFT_RIGHT_THRESHOLD = 100
    }

    private var xDown: Float = 0f
    private var xUp: Float = 0f
    private var yDown: Float = 0f
    private var yUp: Float = 0f
    private var xMove: Float = 0f
    private var yMove: Float = 0f
    private var swipeListener: SwipeListener? = null

    /**
     * Adds listener for swipe events.
     * Remember to call {@link #dispatchTouchEvent(MotionEvent) dispatchTouchEvent} method as well.
     *
     * @param swipeListener listener
     */
    fun setListener(swipeListener: SwipeListener) {
        this.swipeListener = swipeListener
    }

    /**
     * Called to process touch screen events.
     *
     * @param event MotionEvent
     */
    fun dispatchTouchEvent(event: MotionEvent?): Boolean {
        var isEventConsumed = false

        event?.let {
            when (event.action) {
            // user started touching the screen
                MotionEvent.ACTION_DOWN -> onActionDown(event)
            // user stopped touching the screen
                MotionEvent.ACTION_UP -> isEventConsumed = onActionUp(event)
            // user is moving finger on the screen
                MotionEvent.ACTION_MOVE -> onActionMove(event)
            }
        }

        return isEventConsumed
    }

    private fun onActionDown(event: MotionEvent) {
        xDown = event.x
        yDown = event.y
    }

    private fun onActionUp(event: MotionEvent): Boolean {
        xUp = event.x
        yUp = event.y
        val swipedHorizontally = Math.abs(xUp - xDown) > swipedLeftRightThreshold
        val swipedVertically = Math.abs(yUp - yDown) > swipedUpDownThreshold
        var isEventConsumed = false

        if (swipedHorizontally) {
            val swipedRight = xUp > xDown
            val swipedLeft = xUp < xDown

            if (swipedRight) {
                isEventConsumed = swipeListener?.onSwipedRight(event) ?: false
            }
            if (swipedLeft) {
                isEventConsumed = isEventConsumed or (swipeListener?.onSwipedLeft(event) ?: false)
            }
        }

        if (swipedVertically) {
            val swipedDown = yDown < yUp
            val swipedUp = yDown > yUp
            if (swipedDown) {
                isEventConsumed = isEventConsumed or (swipeListener?.onSwipedDown(event, yUp - yDown) ?: false)
            }
            if (swipedUp) {
                isEventConsumed = isEventConsumed or (swipeListener?.onSwipedUp(event, yDown - yUp) ?: false)
            }
        }

        return isEventConsumed
    }

    private fun onActionMove(event: MotionEvent) {
        xMove = event.x
        yMove = event.y
        val isSwipingHorizontally = Math.abs(xMove - xDown) > swipingThreshold
        val isSwipingVertically = Math.abs(yMove - yDown) > swipingThreshold

        if (isSwipingHorizontally) {
            val isSwipingRight = xMove > xDown
            val isSwipingLeft = xMove < xDown

            if (isSwipingRight) {
                swipeListener?.onSwipingRight(event)
            }
            if (isSwipingLeft) {
                swipeListener?.onSwipingLeft(event)
            }
        }

        if (isSwipingVertically) {
            val isSwipingDown = yDown < yMove
            val isSwipingUp = yDown > yMove

            if (isSwipingDown) {
                swipeListener?.onSwipingDown(event)
            }
            if (isSwipingUp) {
                swipeListener?.onSwipingUp(event)
            }
        }
    }

//    private var emitter: ObservableEmitter<SwipeEvent>? = null

//    /**
//     * Observes swipe events with RxJava Observable.
//     * Remember to call {@link #dispatchTouchEvent(MotionEvent) dispatchTouchEvent} method as well.
//     *
//     * @return Observable<SwipeEvent> observable with stream of swipe events
//     */
//    fun observe(): Observable<SwipeEvent> {
//        this.swipeListener = createReactiveSwipeListener()
//        return Observable.create { emitter -> this@SwipeDetector.emitter = emitter }
//    }

//    private fun createReactiveSwipeListener(): SwipeListener {
//        return object : SwipeListener {
//            override fun onSwipingLeft(event: MotionEvent?) {
//                emitter?.onNext(SwipeEvent.SWIPING_LEFT)
//            }
//
//            override fun onSwipedLeft(event: MotionEvent?): Boolean {
//                emitter?.onNext(SwipeEvent.SWIPED_LEFT)
//                return false
//            }
//
//            override fun onSwipingRight(event: MotionEvent?) {
//                emitter?.onNext(SwipeEvent.SWIPING_RIGHT)
//            }
//
//            override fun onSwipedRight(event: MotionEvent?): Boolean {
//                emitter?.onNext(SwipeEvent.SWIPED_RIGHT)
//                return false
//            }
//
//            override fun onSwipingUp(event: MotionEvent?) {
//                emitter?.onNext(SwipeEvent.SWIPING_UP)
//            }
//
//            override fun onSwipedUp(event: MotionEvent?, swipeLength: Float): Boolean {
//                emitter?.onNext(SwipeEvent.SWIPED_UP)
//                return false
//            }
//
//            override fun onSwipingDown(event: MotionEvent?) {
//                emitter?.onNext(SwipeEvent.SWIPING_DOWN)
//            }
//
//            override fun onSwipedDown(event: MotionEvent?, swipeLength: Float): Boolean {
//                emitter?.onNext(SwipeEvent.SWIPED_DOWN)
//                return false
//            }
//        }
//    }

    interface SwipeListener {
        fun onSwipingLeft(event: MotionEvent?)
        fun onSwipingRight(event: MotionEvent?)
        fun onSwipingUp(event: MotionEvent?)
        fun onSwipingDown(event: MotionEvent?)
        fun onSwipedLeft(event: MotionEvent?): Boolean
        fun onSwipedRight(event: MotionEvent?): Boolean
        fun onSwipedUp(event: MotionEvent?, swipeLength: Float): Boolean
        fun onSwipedDown(event: MotionEvent?, swipeLength: Float): Boolean
    }

    enum class SwipeEvent {
        SWIPING_LEFT,
        SWIPED_LEFT,
        SWIPING_RIGHT,
        SWIPED_RIGHT,
        SWIPING_UP,
        SWIPED_UP,
        SWIPING_DOWN,
        SWIPED_DOWN
    }
}