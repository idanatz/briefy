package com.briefy.view

import android.Manifest
import android.graphics.Color
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import com.afollestad.assent.Permission
import com.afollestad.assent.coroutines.awaitPermissionsGranted
import com.afollestad.assent.coroutines.awaitPermissionsResult
import com.briefy.R
import com.briefy.ThemeEngine
import com.briefy.databinding.ActivityMainBinding
import com.briefy.utils.disposeBy
import com.briefy.utils.ui.setItemBackgroundColor
import com.briefy.utils.ui.setItemTextColors
import com.briefy.utils.ui.setStatusBarColor
import com.briefy.utils.ui.viewBinding
import com.briefy.viewmodel.MainViewModel
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject


class MainActivity : AppCompatActivity() {

	private val binding by viewBinding(ActivityMainBinding::inflate)

	private val viewModel: MainViewModel by viewModels()
	private val themeEngine: ThemeEngine by inject()

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
	private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
		setContentView(binding.rootView)

		initNavigation()
		initNavigationDrawer()
        observeTheme()
    }

	override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.takeIf { !it.isDisposed }?.dispose()
    }

	private fun initNavigation() {
		navController = (supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment).findNavController()
	}

	private fun initNavigationDrawer() {
		binding.navigationView.run {
			setupWithNavController(navController)
			setItemTextColors(unselectedColor = Color.BLACK, selectedColor = Color.WHITE)
		}
	}

    private fun observeTheme() {
        themeEngine.run {
            themeRelay.distinctUntilChanged().subscribe { theme ->
                setStatusBarColor(theme.statusBarColor)
				binding.navigationView.setItemBackgroundColor(selectedColor = theme.mainColor)
            }
            .disposeBy(compositeDisposable)
        }
    }

	fun openDrawer() {
		binding.root.openDrawer(binding.navigationView)
	}

	override fun onBackPressed() {
		if (binding.root.isDrawerOpen(GravityCompat.START)) {
			binding.root.closeDrawer(GravityCompat.START)
		} else {
			super.onBackPressed()
		}
	}
}