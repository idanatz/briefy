package com.briefy.view.home.models

data class CurrentLocationUiModel(
		val cityName: String,
        val countryName: String
)