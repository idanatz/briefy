package com.briefy.view.home.models

import com.briefy.model.WeatherInfo

class CurrentForecastUiModel(
		val averageTemp: Int,
		val minTemperature: Int,
		val maxTemperature: Int,
		val description: String,
		val isDayTime: Boolean,
		val weatherType: WeatherInfo.Type
)