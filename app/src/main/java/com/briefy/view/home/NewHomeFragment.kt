package com.briefy.view.home

import android.os.Bundle
import android.view.*
import android.widget.Toast
import com.briefy.R
import com.briefy.view.base.BaseFragment
import com.briefy.viewmodel.HomeFragmentViewModel
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.afollestad.assent.Permission
import com.afollestad.assent.coroutines.awaitPermissionsResult
import com.briefy.ThemeEngine
import com.briefy.databinding.FragmentNewHomeBinding
import com.briefy.model.UiState
import com.briefy.utils.*
import com.briefy.utils.ui.viewBinding
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class NewHomeFragment : BaseFragment(R.layout.fragment_new_home) {

	private val binding by viewBinding(FragmentNewHomeBinding::bind)

	private val viewModel: HomeFragmentViewModel by viewModels()
	private val themeEngine: ThemeEngine by inject()

//	private lateinit var futureForecastsViews: List<FutureForecastView>

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		bindViews()

		attachClickListeners()
		observeTheme()

		observeViewModel()
		checkLocationPermission()
	}

	private fun checkLocationPermission() {
		lifecycleScope.launch {
			val permissions = arrayOf(Permission.ACCESS_FINE_LOCATION, Permission.ACCESS_BACKGROUND_LOCATION)
			val result = awaitPermissionsResult(*permissions)
			if (result.isAllGranted(*permissions)) {
				viewModel.onLocationPermissionGranted()
			} else {
				// @TODO: 23/12/2020 handle
			}
		}
	}

    private fun bindViews() {
//		futureForecastsViews = listOf(binding.forecastDay1, binding.forecastDay2, binding.forecastDay3)
    }

    private fun attachClickListeners() {
        binding.navigationDrawerButton.setOnClickListener { activity?.openDrawer() }
    }

    private fun observeViewModel() {
        viewModel.run {
            currentLocationRelay.distinctUntilChanged().subscribe { state ->
                when (state) {
                    is UiState.Loading -> {
//                        when (state.isLoading) {
//                            true -> binding.locationView.startSearchingAnimation()
//                            false -> binding.locationView.endSearchingAnimation()
//                        }
                    }
                    is UiState.Data -> binding.locationView.setData(state.data.cityName, state.data.countryName)
                    is UiState.Error -> Toast.makeText(activity, state.reason, Toast.LENGTH_LONG).show()
                }
            }
            .disposeBy(compositeDisposable)

            currentForecastRelay.distinctUntilChanged().subscribe { state ->
                when (state) {
                    is UiState.Loading -> { } // TODO: finish this
					is UiState.Data -> {
						state.data.run {
							binding.forecastAverageTemp.text = getString(R.string.temperature).format(averageTemp)
							binding.forecastWeatherInfoText.text = description
//
//                            // original view
//                            currForecastDescription.value.text = description
//                            currForecastTempRange.value.text = activity?.run { getString(R.string.temperature_range).format(minTemperature, maxTemperature) }
//
//                            // collapsed view
//                            collapsedDescriptionText.value.text = description
//
//                            // theme-ing
							themeEngine.init(weatherType, isDayTime) // @TODO: 24/11/2020 change this to be real reactive
//                            binding.forecastIcon.setImageDrawable(themeEngine.getWeatherIcon(weatherType, isAfterSunset, R.color.white))
//                            currForecastTempRangeIcon.value.setImageDrawable(themeEngine.getIcon(WeatherIcons.Icon.wic_thermometer, R.color.white))
//
//                            originalView.value.takeUnless { firstLoadFinished }?.post { transition = Transitioner(originalView.value, endingView.value) }
//                            firstLoadFinished = true
						}
					}
                    is UiState.Error -> Toast.makeText(activity, state.reason, Toast.LENGTH_LONG).show()
                }
            }
            .disposeBy(compositeDisposable)

            futureForecastRelay.distinctUntilChanged().subscribe { state ->
                when (state) {
//                    is UiState.Loading -> {  // TODO: finish this
//                        futureForecastsViews.forEach {
//                            it.setLoading(state.isLoading)
//                        }
//                    }
//                    is UiState.Data -> {
//                        state.data.forecasts.zip(futureForecastsViews) { forecast, view ->
//                            activity?.run {
//                                val weatherIcon = themeEngine.getWeatherIcon(forecast.weatherType ?: WeatherInfo.Type.Unknown, color = R.color.white)
//                                val temperature = let2(forecast.minTemperature, forecast.maxTemperature) { minTemperature, maxTemperature ->
//                                    if (minTemperature == maxTemperature) getString(R.string.temperature).format(minTemperature)
//                                    else getString(R.string.temperature_range).format(minTemperature, maxTemperature)
//                                } ?: let {
//                                    "N/A"
//                                }
//                                view.setForecast(forecast.dayOfTheWeek, temperature, weatherIcon)
//                            }
//                        }
//                    }
//                    is UiState.Error -> Toast.makeText(activity, state.reason, Toast.LENGTH_LONG).show()
                }
            }
            .disposeBy(compositeDisposable)
        }
    }

    private fun observeTheme() {
        themeEngine.run {
			themeRelay.distinctUntilChanged().subscribe { theme ->
                binding.forecastBackground.setImageDrawable(theme.weatherBackground)
            }
            .disposeBy(compositeDisposable)
        }
    }
}