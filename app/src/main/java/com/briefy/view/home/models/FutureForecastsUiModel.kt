package com.briefy.view.home.models

import com.briefy.model.WeatherInfo

data class FutureForecastsUiModel(
        val forecasts: MutableList<FutureForecastUiModel>
) {
    data class FutureForecastUiModel(
            val dayOfTheWeek: String,
            val maxTemperature: Int?,
            val minTemperature: Int?,
            val weatherType: WeatherInfo.Type?
    )
}