package com.briefy.view.home.views

import android.content.Context
import androidx.constraintlayout.widget.ConstraintLayout
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.postDelayed
import com.briefy.R
import com.briefy.databinding.ViewCurrentLocationBinding
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo

class CurrentLocationView : ConstraintLayout {

	private val binding = ViewCurrentLocationBinding.inflate(LayoutInflater.from(context), this)

    private var shouldStopLocationIconAnimation: Boolean = false

	constructor(context: Context) : super(context)
	constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
	constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    fun setData(cityName: String, countryName: String) {
		binding.cityName.text = cityName
		binding.countryName.text = countryName
    }

    fun startSearchingAnimation() {
        YoYo.with(Techniques.Bounce)
                .duration(1500)
                .onEnd {
                    if (!shouldStopLocationIconAnimation) {
                        startSearchingAnimation()
                    }
                }
                .playOn(binding.locationIcon)
    }

    fun endSearchingAnimation() {
        shouldStopLocationIconAnimation = true
    }
}