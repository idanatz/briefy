package com.briefy.view.home.views

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.constraintlayout.widget.ConstraintLayout
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import com.briefy.databinding.FutureForecastBinding
import com.briefy.model.UiState
import com.briefy.utils.ui.addListener
import com.briefy.utils.ui.animateAlpha
import kotlin.properties.Delegates.observable

class FutureForecastView : ConstraintLayout {

	private val binding = FutureForecastBinding.inflate(LayoutInflater.from(context), this)
	private var loadingState: UiState.Loading = UiState.Loading(false)

	constructor(context: Context) : super(context)
	constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
	constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

	fun setForecast(dayOfTheWeek:String, temperature: String, weatherIcon: Drawable) {
		binding.dayOfTheWeek.text = dayOfTheWeek
		binding.weatherIcon.setImageDrawable(weatherIcon)
		binding.temperature.text = temperature
	}

	fun setLoading(value: Boolean) {
		if (loadingState.isLoading == value) return

		when (value) {
			true -> {
				loadingState = UiState.Loading(true)
				animateAlpha(0f, 1f) {
					addListener(
							animationStart = {
								binding.progressBar.visibility = View.VISIBLE
								binding.weatherIcon.visibility = View.INVISIBLE
								binding.temperature.visibility = View.INVISIBLE
							}
					)
					setAutoCancel(true)
					duration = 500
				}
			}
			false -> {
				loadingState = UiState.Loading(false)
				animateAlpha(0f, 1f) {
					addListener(
							animationStart = {
								binding.progressBar.visibility = View.INVISIBLE
								binding.weatherIcon.visibility = View.VISIBLE
								binding.temperature.visibility = View.VISIBLE
							}
					)
					setAutoCancel(true)
					duration = 500
				}
			}
		}
	}
}