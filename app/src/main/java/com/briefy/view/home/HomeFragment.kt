package com.briefy.view.home

import android.os.Bundle
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.Guideline
import android.view.*
import android.widget.Toast
import bg.devlabs.transitioner.Transitioner
import com.briefy.R
import com.briefy.view.base.BaseFragment
import com.briefy.view.custom.ScrollViewEnhanced
import com.briefy.viewmodel.HomeFragmentViewModel
import me.grantland.widget.AutofitTextView
import android.view.animation.DecelerateInterpolator
import android.widget.ImageView
import androidx.fragment.app.FragmentContainerView
import androidx.fragment.app.viewModels
import com.briefy.ThemeEngine
import com.briefy.databinding.FragmentHomeBinding
import com.briefy.model.UiState
import com.briefy.model.WeatherInfo
import com.briefy.utils.*
import com.briefy.utils.ui.animateInt
import com.briefy.utils.ui.getVisibleRect
import com.briefy.utils.ui.viewBinding
import com.briefy.view.home.views.FutureForecastView
import com.briefy.view.home.views.CurrentLocationView
import com.mikepenz.weather_icons_typeface_library.WeatherIcons
import org.koin.android.ext.android.inject
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent

class HomeFragment : BaseFragment(R.layout.fragment_home) {

	private val binding by viewBinding(FragmentHomeBinding::bind)

	//region Views
    // main view
    private lateinit var originalView: Lazy<ConstraintLayout>
    private lateinit var locationView: Lazy<CurrentLocationView>
    private lateinit var navigationDrawerButton: Lazy<ImageView>
    private lateinit var currForecastIcon: Lazy<ImageView>
    private lateinit var currForecastTempRangeIcon: Lazy<ImageView>
    private lateinit var currForecastAverageTemp: Lazy<AutofitTextView>
    private lateinit var currForecastTempRange: Lazy<AutofitTextView>
    private lateinit var currForecastDescription: Lazy<AutofitTextView>
    private lateinit var forecastBackground: Lazy<ImageView>

    // collapsed view
    private lateinit var endingView: Lazy<ConstraintLayout>
    private lateinit var collapsedAverageTempText: Lazy<AutofitTextView>
    private lateinit var collapsedDescriptionText: Lazy<AutofitTextView>

    // view lists
    private lateinit var collapseTopViews: List<Lazy<View>>
    private lateinit var futureForecastsViews: List<Lazy<FutureForecastView>>

    private lateinit var scrollView: Lazy<ScrollViewEnhanced>
    private lateinit var guideline: Lazy<Guideline>
	private var fragmentContainerView: FragmentContainerView? = null
    //endregion

    //region Dependency Injection
	private val viewModel: HomeFragmentViewModel by viewModels()
	private val themeEngine: ThemeEngine by inject()
    //endregion

    private var transition: Transitioner? = null
    private var totalCollapseLength: Int = 0
    private var firstLoadFinished: Boolean = false

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		bindViews(view)

		initScrollView()
		attachClickListeners()
		observeTheme()

		observeViewModel()
	}

    private fun bindViews(view: View) {
        originalView = lazy { view.findViewById<ConstraintLayout>(R.id.original_view) }
        endingView = lazy { view.findViewById<ConstraintLayout>(R.id.ending_view) }
        locationView = lazy { view.findViewById<CurrentLocationView>(R.id.location_view) }
        navigationDrawerButton = lazy { view.findViewById<ImageView>(R.id.navigation_drawer_button) }
        currForecastIcon = lazy { view.findViewById<ImageView>(R.id.forecast_icon) }
        currForecastTempRangeIcon = lazy { view.findViewById<ImageView>(R.id.forecast_temperature_range_icon) }
        currForecastAverageTemp = lazy { view.findViewById<AutofitTextView>(R.id.forecast_average_temp) }
        currForecastTempRange = lazy { view.findViewById<AutofitTextView>(R.id.forecast_temperature_range) }
        currForecastDescription = lazy { view.findViewById<AutofitTextView>(R.id.forecast_description) }
        forecastBackground = lazy { view.findViewById<ImageView>(R.id.forecast_background) }
        collapsedAverageTempText = lazy { view.findViewById<AutofitTextView>(R.id.collapsed_temp) }
        collapsedDescriptionText = lazy { view.findViewById<AutofitTextView>(R.id.collapsed_description) }
        scrollView = lazy { view.findViewById<ScrollViewEnhanced>(R.id.scroll_view) }
        guideline = lazy { view.findViewById<Guideline>(R.id.home_controller_toolbar_guideline) }
		fragmentContainerView = binding.navHostFragment

        collapseTopViews = listOf(locationView, navigationDrawerButton)
        futureForecastsViews = listOf(
                lazy { view.findViewById<FutureForecastView>(R.id.forecast_day1) },
                lazy { view.findViewById<FutureForecastView>(R.id.forecast_day2) },
                lazy { view.findViewById<FutureForecastView>(R.id.forecast_day3) }
        )
    }

    private fun initScrollView() {
        // set percentage padding from top for the child controller
        scrollView.value.setPadding(0, screenHeight.percent(80f).toInt(), 0, 0)

        runWithDelay(500) {
            totalCollapseLength = fragmentContainerView!!.height - (screenHeight - fragmentContainerView!!.getVisibleRect().top)

            scrollView.value.setScrollViewListener(object : ScrollViewEnhanced.ScrollViewListener {
                val toolbarGuidelinePercentage = (guideline.value.layoutParams as ConstraintLayout.LayoutParams).guidePercent
                var prevPercentage = 0f

                override fun onScrollChanged(scrollView: ScrollViewEnhanced, x: Float, y: Float, oldx: Float, oldy: Float) {
                    var percentage = y / totalCollapseLength
                    percentage = percentage.coerceIn(0f, 1f)

                    if (prevPercentage != percentage) {
                        transition?.setProgress(percentage)

                        // move the toolbar views
                        guideline.value.layoutParams = (guideline.value.layoutParams as ConstraintLayout.LayoutParams).apply {
                            guidePercent = toolbarGuidelinePercentage - (percentage / 10)
                        }

                        // fade in / out the toolbar views
                        collapseTopViews.forEach {
                            it.value.alpha = 1f - (percentage * 1.5f)
                            it.value.isEnabled = percentage in 0f..0.2f
                        }

                        // fade in / out the next days forecasts
                        futureForecastsViews.forEach {
                            it.value.alpha = 1f - (percentage * 1.7f)
                        }
                    }

                    when (percentage) {
                        1f -> scrollView.scrollState = ScrollViewEnhanced.ScrollState.Collapsed
                        0f -> scrollView.scrollState = ScrollViewEnhanced.ScrollState.Full
                        else -> scrollView.scrollState = ScrollViewEnhanced.ScrollState.MidCollapse
                    }

                    prevPercentage = percentage
                }

                override fun onSwipedUp(swipeLength: Float): Boolean {
                    return if (scrollView.value.scrollState == ScrollViewEnhanced.ScrollState.MidCollapse) {
                        when (swipeLength / totalCollapseLength) {
                            in 0f..0.25f -> expendView()
                            else -> collapseView()
                        }
                        true
                    } else false
                }

                override fun onSwipedDown(swipeLength: Float): Boolean {
                    return if (scrollView.value.scrollState == ScrollViewEnhanced.ScrollState.MidCollapse) {
                        when (swipeLength / totalCollapseLength) {
                            in 0f..0.25f -> collapseView()
                            else -> expendView()
                        }
                        true
                    } else false
                }
            })
        }
    }

    private fun attachClickListeners() {
        navigationDrawerButton.value.setOnClickListener { activity?.openDrawer() }
    }

    private fun observeViewModel() {
        viewModel.run {
            currentLocationRelay.distinctUntilChanged().subscribe { state ->
                when (state) {
                    is UiState.Loading -> {
                        when (state.isLoading) {
                            true -> locationView.value.startSearchingAnimation()
                            false -> locationView.value.endSearchingAnimation()
                        }
                    }
                    is UiState.Data -> locationView.value.setData(state.data.cityName, state.data.countryName)
                    is UiState.Error -> Toast.makeText(activity, state.reason, Toast.LENGTH_LONG).show()
                }
            }
            .disposeBy(compositeDisposable)

            currentForecastRelay.distinctUntilChanged().subscribe { state ->
                when (state) {
                    is UiState.Loading -> { } // TODO: finish this
                    is UiState.Data -> {
                        state.data.run {
                            val temperatureString = "$averageTemp°"

                            // original view
                            currForecastAverageTemp.value.text = temperatureString
                            currForecastDescription.value.text = description
                            currForecastTempRange.value.text = activity?.run { getString(R.string.temperature_range).format(minTemperature, maxTemperature) }

                            // collapsed view
                            collapsedAverageTempText.value.text = temperatureString
                            collapsedDescriptionText.value.text = description

                            // theme-ing
                            themeEngine.init(weatherType, isDayTime)
                            currForecastIcon.value.setImageDrawable(themeEngine.getWeatherIcon(weatherType, isDayTime, R.color.white))
                            currForecastTempRangeIcon.value.setImageDrawable(themeEngine.getIcon(WeatherIcons.Icon.wic_thermometer, R.color.white))

                            originalView.value.takeUnless { firstLoadFinished }?.post { transition = Transitioner(originalView.value, endingView.value) }
                            firstLoadFinished = true
                        }
                    }
                    is UiState.Error -> Toast.makeText(activity, state.reason, Toast.LENGTH_LONG).show()
                }
            }
            .disposeBy(compositeDisposable)

            futureForecastRelay.distinctUntilChanged().subscribe { state ->
                when (state) {
                    is UiState.Loading -> {  // TODO: finish this
                        futureForecastsViews.forEach {
                            it.value.setLoading(state.isLoading)
                        }
                    }
                    is UiState.Data -> {
                        state.data.forecasts.zip(futureForecastsViews) { forecast, view ->
                            activity?.run {
                                val weatherIcon = themeEngine.getWeatherIcon(forecast.weatherType ?: WeatherInfo.Type.Unknown, color = R.color.white)
                                val temperature = let2(forecast.minTemperature, forecast.maxTemperature) { minTemperature, maxTemperature ->
                                    if (minTemperature == maxTemperature) getString(R.string.temperature).format(minTemperature)
                                    else getString(R.string.temperature_range).format(minTemperature, maxTemperature)
                                } ?: let {
                                    "N/A"
                                }
                                view.value.setForecast(forecast.dayOfTheWeek, temperature, weatherIcon)
                            }
                        }
                    }
                    is UiState.Error -> Toast.makeText(activity, state.reason, Toast.LENGTH_LONG).show()
                }
            }
            .disposeBy(compositeDisposable)
        }
    }

    private fun observeTheme() {
        themeEngine.run {
			themeRelay.distinctUntilChanged().subscribe { theme ->
                forecastBackground.value.setImageDrawable(theme.weatherBackground)
            }
            .disposeBy(compositeDisposable)
        }
    }

    private fun expendView() {
        scrollView.value.animateInt(from = scrollView.value.scrollY, to = 0) {
            interpolator = DecelerateInterpolator()
            duration = 250
            addUpdateListener { animationValue -> scrollView.value.smoothScrollTo(scrollView.value.scrollX, animationValue.animatedValue as Int) }
        }.start()
    }

    private fun collapseView() {
        scrollView.value.animateInt(from = scrollView.value.scrollY, to = totalCollapseLength) {
            interpolator = DecelerateInterpolator()
            duration = 250
            addUpdateListener { animationValue -> scrollView.value.smoothScrollTo(scrollView.value.scrollX, animationValue.animatedValue as Int) }
        }.start()
    }

	override fun onDestroyView() {
		scrollView.value.clearScrollViewListener()
		firstLoadFinished = false
		super.onDestroyView()
	}
}