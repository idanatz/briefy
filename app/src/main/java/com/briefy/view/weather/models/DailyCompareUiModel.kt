package com.briefy.view.weather.models

import com.briefy.model.WeatherInfo

data class HourlyForecastCompareUiModel(
		val timestamp: Long,
		val isCurrentTime: Boolean = false,
		var yesterdayForecast: DailyCompareUiModel?,
		var todayForecast: DailyCompareUiModel?
) {
    data class DailyCompareUiModel(
            val averageTemperature: Int,
            val weatherType: WeatherInfo.Type
    )
}