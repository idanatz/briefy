package com.briefy.view.weather.views

import android.content.Context
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import androidx.constraintlayout.widget.ConstraintLayout
import android.util.AttributeSet
import android.view.LayoutInflater
import com.briefy.R
import com.briefy.databinding.ViewForecastCompareColumnBinding
import com.briefy.utils.getColorCompat
import com.mikepenz.iconics.IconicsDrawable

class ForecastCompareColumnView : ConstraintLayout {

	private val binding = ViewForecastCompareColumnBinding.inflate(LayoutInflater.from(context), this)

	constructor(context: Context) : super(context)
	constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
	constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

	fun setTodayForecast(forecastIcon: Drawable, temperature: String = "") {
        binding.todayTemperature.text = temperature
		binding.todayImage.setImageDrawable(forecastIcon)
    }

    fun setYesterdayForecast(forecastIcon: Drawable, temperature: String = "") {
		binding.yesterdayTemperature.text = temperature
		binding.yesterdayImage.setImageDrawable(forecastIcon)
    }

    fun setForecastsColors(color: Int) {
		binding.todayImage.drawable?.let { todayDrawable ->
            (todayDrawable as IconicsDrawable).color(color)
        }
    }

	fun setTime(time: String) {
		binding.time.text = time
	}

    fun setTimeAsCurrent() {
        val highlightColor = context.getColorCompat(R.color.black)

        binding.time.run {
			setTextColor(highlightColor)
			typeface = Typeface.DEFAULT_BOLD
		}
		binding.todayTemperature.setTextColor(highlightColor)
		binding.yesterdayTemperature.setTextColor(highlightColor)
    }
}