package com.briefy.view.weather

sealed class WeatherFragmentUiActions {
	class ShowErrorMessage(val msg: String) : WeatherFragmentUiActions()
}