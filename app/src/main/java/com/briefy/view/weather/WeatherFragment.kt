package com.briefy.view.weather

import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.view.children
import androidx.fragment.app.viewModels
import com.briefy.R
import com.briefy.ThemeEngine
import com.briefy.databinding.FragmentWeatherBinding
import com.briefy.model.UiState
import com.briefy.model.WeatherInfo
import com.briefy.screen.location.view.LocationFragmentUiActions
import com.briefy.utils.disposeBy
import com.briefy.utils.getColorCompat
import com.briefy.utils.percent
import com.briefy.utils.toDateInFormat
import com.briefy.utils.ui.replacedViewsWith
import com.briefy.utils.ui.setHeight
import com.briefy.utils.ui.toVisibility
import com.briefy.utils.ui.viewBinding
import com.briefy.view.base.BaseFragment
import com.briefy.view.weather.WeatherFragmentUiStates.Main
import com.briefy.view.weather.views.HourlyForecastView
import com.briefy.viewmodel.WeatherFragmentViewModel
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import org.koin.android.ext.android.inject

class WeatherFragment : BaseFragment(R.layout.fragment_weather) {

	private val binding by viewBinding(FragmentWeatherBinding::bind)

	private val viewModel: WeatherFragmentViewModel by viewModels()
	private val themeEngine: ThemeEngine by inject()

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

        view.setHeight(screenHeight.percent(80f).toInt())
        initChart()

        observeViewModel()
        observeTheme()

        viewModel.viewReady()
    }

    private fun initChart() {
        binding.chart.run {
            description.isEnabled = false
            setDrawGridBackground(false)
            setTouchEnabled(false)
            setViewPortOffsets(60f, 70f, 60f, 0f)
            legend.verticalAlignment = Legend.LegendVerticalAlignment.TOP

            // remove right and left axises
            axisLeft.isEnabled = false
            axisRight.isEnabled = false

            // x axis modification
            xAxis.run {
                position = XAxis.XAxisPosition.BOTTOM
                setDrawGridLines(false)
                setDrawAxisLine(false)
                granularity = 1f
                axisMinimum = 0f
                axisMaximum = 8f
            }
        }
    }

    private fun observeViewModel() {
        viewModel.run {
			uiStateObservable.subscribe { state ->
				when (state) {
					is Main -> {
						when (val forecastLocationState = state.forecastLocation) {
							is UiState.Data -> binding.location.text = forecastLocationState.data
						}

						when (val forecastSummeryState = state.forecastSummery) {
							is UiState.Loading -> { } // @TODO: 25/11/2020 handle the new ui change
							is UiState.Data -> binding.summery.text = forecastSummeryState.data
							is UiState.Error -> binding.summery.text = forecastSummeryState.reason
						}

						when (val forecastsComparison = state.forecastsComparison) {
							is UiState.Loading -> {
								binding.forecastCompareListProgressBar.visibility = forecastsComparison.isLoading.toVisibility()
							}
							is UiState.Data -> {
								val todayEntries = mutableListOf<Entry>()
								val yesterdayEntries = mutableListOf<Entry>()

								val hourlyForecastViews = forecastsComparison.data.mapIndexed { index, (timestamp, isCurrentTime, yesterdayForecast, todayForecast) ->
									val hourlyForecastView = HourlyForecastView(binding.todayHourlyComparesContainer.context).apply {
										layoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f)
									}

									hourlyForecastView.setTime(timestamp.toDateInFormat("HH:mm"))

									todayForecast?.let {
										todayEntries.add(Entry(index.toFloat(), it.averageTemperature.toFloat()))
										hourlyForecastView.setForecast(
												forecastIcon = themeEngine.getWeatherIcon(it.weatherType),
												temperature = "${it.averageTemperature}°")
									} ?: let {
										hourlyForecastView.setForecast(
												forecastIcon = themeEngine.getWeatherIcon(WeatherInfo.Type.Unknown))
									}

									yesterdayForecast?.let {
										yesterdayEntries.add(Entry(index.toFloat(), it.averageTemperature.toFloat()))
//										currentColumn.value.setYesterdayForecast(
//												forecastIcon = themeEngine.getWeatherIcon(it.weatherType, color = R.color.grey_400),
//												temperature = "${it.averageTemperature}°")
									} ?: let {
//										currentColumn.value.setYesterdayForecast(
//												forecastIcon = themeEngine.getWeatherIcon(WeatherInfo.Type.Unknown, color = R.color.grey_400))
									}

									if (isCurrentTime) {
										hourlyForecastView.setTimeAsCurrent()
									}

									hourlyForecastView
								}.toList()

								binding.todayHourlyComparesContainer.replacedViewsWith(hourlyForecastViews)
								setChartData(yesterdayEntries, todayEntries)
							}
							is UiState.Error -> { }
						}
					}
				}
			}
			.disposeBy(compositeDisposable)

			uiActionObservable.subscribe { action ->
				when (action) {
					is WeatherFragmentUiActions.ShowErrorMessage -> Toast.makeText(activity, action.msg, Toast.LENGTH_SHORT).show()
				}
			}.disposeBy(compositeDisposable)
        }
    }

    private fun observeTheme() {
        themeEngine.run {
            themeRelay.distinctUntilChanged().subscribe { theme ->
                binding.location.setTextColor(theme.mainColor)

				binding.todayHourlyComparesContainer.children.filterIsInstance<HourlyForecastView>().forEach {
					it.setForecastsColors(theme.mainColor)
				}

				binding.separator.setBackgroundColor(theme.subColor)

				binding.chart.data?.getDataSetByLabel("Today", false).takeIf { it is LineDataSet }?.let {
                    (it as LineDataSet).run {
                        setColors(theme.mainColor)
                        fillDrawable = theme.gradient
                    }
                }
            }
            .disposeBy(compositeDisposable)
        }
    }

    private fun setChartData(yesterdayEntries: MutableList<Entry>, todayEntries: MutableList<Entry>) {
        activity?.run context@ {
            val chartDataSet = LineData().apply {
                yesterdayEntries.takeUnless { it.isEmpty() }?.let {
                    addDataSet(LineDataSet(yesterdayEntries, "Yesterday").apply {
						setColors(this@context.getColorCompat(R.color.grey_400))
						fillColor = this@context.getColorCompat(R.color.grey_400)
						valueTextColor = this@context.getColorCompat(R.color.grey_400)
					})
                }

                todayEntries.takeUnless { it.isEmpty() }?.let {
                    addDataSet(LineDataSet(todayEntries, "Today").apply {
						setColors(themeEngine.themeRelay.value.mainColor)
						fillDrawable = themeEngine.themeRelay.value.gradient
					})
                }

                dataSets.forEach {
                    (it as LineDataSet).apply {
                        axisDependency = YAxis.AxisDependency.LEFT
                        lineWidth = 1f
                        valueTextSize = 8f
                        setDrawCircles(false)
                        mode = LineDataSet.Mode.HORIZONTAL_BEZIER
                        setDrawFilled(true)
                    }
                }

                setValueFormatter { value, _, _, _ -> "${value.toInt()}°" }
            }

			binding.chart.run {
				// the way it looks on the screen
				setViewPortOffsets(10f, 0f, 10f, 45f)
				xAxis.axisMaximum = chartDataSet.xMax
				xAxis.axisMinimum = chartDataSet.xMin
				xAxis.setDrawLabels(false)

				legend.let {
					it.orientation = Legend.LegendOrientation.HORIZONTAL
					it.verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
					it.horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT
					it.setDrawInside(false)
				}

				data = chartDataSet

				// animation
				animateY(2000, Easing.EasingOption.EaseOutCubic)
			}
        }
    }
}