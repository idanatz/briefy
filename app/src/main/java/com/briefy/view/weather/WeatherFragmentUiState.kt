package com.briefy.view.weather

import com.briefy.model.UiState
import com.briefy.view.weather.models.HourlyForecastCompareUiModel

sealed class WeatherFragmentUiStates {

	data class Main(
			val forecastLocation: UiState<String> = UiState.Init,
			val forecastSummery: UiState<String> = UiState.Init,
			val forecastsComparison: UiState<List<HourlyForecastCompareUiModel>> = UiState.Init
	) : WeatherFragmentUiStates()
}