package com.briefy.view.base

import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.briefy.utils.*
import com.briefy.view.MainActivity
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import java.util.concurrent.TimeUnit

abstract class BaseFragment(@LayoutRes contentLayoutId: Int) : Fragment(contentLayoutId) {

	protected val compositeDisposable: CompositeDisposable = CompositeDisposable()

    protected val activity: MainActivity?
        get() { return getActivity() as? MainActivity }

    protected val screenWidth: Int by lazy { activity?.displayWidth ?: 0 }
    protected val screenHeight: Int by lazy { activity?.displayHeight ?: 0 }

    protected fun runWithDelay(delay: Long, body: () -> Unit) {
        Single.timer(delay, TimeUnit.MILLISECONDS)
                .observeOnMain()
                .subscribe({
                    body.invoke()
                }, {
                })
                .disposeBy(compositeDisposable)
    }

	override fun onDestroyView() {
		super.onDestroyView()
		compositeDisposable.takeUnless { compositeDisposable.isDisposed }?.run { clear() }
	}
}