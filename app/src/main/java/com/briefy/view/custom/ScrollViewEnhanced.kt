package com.briefy.view.custom

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.ScrollView
import com.briefy.view.helpers.SwipeDetector

/**
 * Created by Idan Atsmon on 23/02/2018.
 */

private const val MAX_SCROLL_SPEED = 5000

class ScrollViewEnhanced : ScrollView, SwipeDetector.SwipeListener {

    private var scrollViewListener: ScrollViewListener? = null
    private val swipeDetector = SwipeDetector(swipedUpDownThreshold = 20, swipedLeftRightThreshold = 200)
    var scrollState: ScrollState = ScrollState.Full

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    init {
        swipeDetector.setListener(this)
    }

    fun setScrollViewListener(scrollViewListener: ScrollViewListener) {
        this.scrollViewListener = scrollViewListener
    }

    fun clearScrollViewListener() {
        scrollViewListener = null
    }

    fun stopScroll() {
        fling(0)
    }

    override fun onScrollChanged(l: Int, t: Int, oldl: Int, oldt: Int) {
        super.onScrollChanged(l, t, oldl, oldt)
        scrollViewListener?.onScrollChanged(this, l.toFloat(), t.toFloat(), oldl.toFloat(), oldt.toFloat())
    }

    override fun fling(velocityY: Int) {
        val topVelocityY = (Math.min(Math.abs(velocityY), MAX_SCROLL_SPEED) * Math.signum(velocityY.toFloat()))
        super.fling(topVelocityY.toInt())
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        swipeDetector.dispatchTouchEvent(ev)
        return super.dispatchTouchEvent(ev)
    }

    override fun onSwipedUp(event: MotionEvent?, swipeLength: Float): Boolean {
        return scrollViewListener?.onSwipedUp(swipeLength) ?: false
    }

    override fun onSwipedDown(event: MotionEvent?, swipeLength: Float): Boolean {
        return scrollViewListener?.onSwipedDown(swipeLength) ?: false
    }

    override fun onSwipedRight(event: MotionEvent?): Boolean {
        return false
    }

    override fun onSwipedLeft(event: MotionEvent?): Boolean {
        return false
    }

    override fun onSwipingLeft(event: MotionEvent?) {}
    override fun onSwipingRight(event: MotionEvent?) {}
    override fun onSwipingDown(event: MotionEvent?) {}
    override fun onSwipingUp(event: MotionEvent?) {}

    interface ScrollViewListener {
        fun onScrollChanged(scrollView: ScrollViewEnhanced, x: Float, y: Float, oldx: Float, oldy: Float)
        fun onSwipedUp(swipeLength: Float): Boolean
        fun onSwipedDown(swipeLength: Float): Boolean
    }

    enum class ScrollState {
        Collapsed, Full, MidCollapse
    }
}