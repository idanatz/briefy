package com.briefy

import android.app.NotificationChannel
import android.content.Context
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat

class PushNotificationManager(
		private val context: Context,
		private val notificationManager: NotificationManagerCompat
) {

    fun sendNotification(title: String, content: String) {
        val builder = NotificationCompat.Builder(context, context.getString(R.string.notification_channel_id))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(content)
				.setStyle(NotificationCompat.BigTextStyle().bigText(content))

		notificationManager.notify(System.currentTimeMillis().toInt(), builder.build())
    }

	fun createNotificationChannel(channel: NotificationChannel) {
		notificationManager.createNotificationChannel(channel)
	}
}