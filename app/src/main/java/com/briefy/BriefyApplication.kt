package com.briefy

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import androidx.work.Configuration
import androidx.work.ExistingWorkPolicy
import androidx.work.WorkManager
import com.briefy.di.*
import com.briefy.network.di.networkModule
import com.briefy.screen.location.di.locationScreenModule
import com.briefy.works.DeleteOldForecastsWork
import com.briefy.works.GetForecastsWork
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import org.koin.core.context.startKoin
import timber.log.Timber

class BriefyApplication : Application(), Configuration.Provider, KoinComponent {

    override fun onCreate() {
        super.onCreate()

        initDependencyInjections()
		initLogging()
        initNotificationChannels()
		initWorks()
    }

	private fun initDependencyInjections() {
		startKoin {
			androidLogger()
			androidContext(this@BriefyApplication)
			modules(listOf(
					applicationModule,
					locationScreenModule,
					localRepositoryModule,
					networkModule,
					persistenceModule,
			))
		}
	}

	private fun initLogging() {
		val timberTree = get<Timber.Tree>()
		Timber.plant(timberTree)
	}

    private fun initNotificationChannels() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.notification_channel_name)
            val description = getString(R.string.notification_channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(getString(R.string.notification_channel_id), name, importance)
            channel.description = description
            // Register the channel with the system; you can't change the importance or other notification behaviors after this
			val pushNotificationManager = get<PushNotificationManager>()
			pushNotificationManager.createNotificationChannel(channel)
        }
    }

	private fun initWorks() {
		val workManager = get<WorkManager>()
		workManager.enqueueUniqueWork(GetForecastsWork.TAG, ExistingWorkPolicy.KEEP, GetForecastsWork.create())
		workManager.enqueueUniqueWork(DeleteOldForecastsWork.TAG, ExistingWorkPolicy.KEEP, DeleteOldForecastsWork.create())
	}

	override fun getWorkManagerConfiguration() =
			Configuration.Builder()
					.setMinimumLoggingLevel(android.util.Log.DEBUG)
					.build()
}