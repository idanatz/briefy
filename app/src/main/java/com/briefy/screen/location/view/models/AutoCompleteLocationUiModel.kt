package com.briefy.screen.location.view.models

import com.idanatz.oneadapter.external.interfaces.Diffable

data class AutoCompleteLocationUiModel(
		val id: Int,
        val cityName: String,
        val countryName: String
) : Diffable {

	override val uniqueIdentifier: Long = id.toLong()

	override fun areContentTheSame(other: Any): Boolean = other is AutoCompleteLocationUiModel && other == this
}