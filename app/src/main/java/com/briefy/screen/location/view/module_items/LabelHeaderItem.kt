package com.briefy.screen.location.view.module_items

import android.widget.TextView
import com.briefy.R
import com.briefy.screen.location.view.models.Label
import com.idanatz.oneadapter.external.modules.ItemModule

class LabelHeaderItem : ItemModule<Label>() {
	init {
		config {
			layoutResource = R.layout.item_label_header
		}
		onBind { model, viewBinder, _ ->
			viewBinder.findViewById<TextView>(R.id.textView).text = model.label
		}
	}
}