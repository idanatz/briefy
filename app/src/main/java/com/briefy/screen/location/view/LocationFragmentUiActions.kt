package com.briefy.screen.location.view

sealed class LocationFragmentUiActions {
	class ShowSuccessMessage(val msg: String) : LocationFragmentUiActions()
	class ShowWarningMessage(val msg: String) : LocationFragmentUiActions()
	class ShowErrorMessage(val msg: String) : LocationFragmentUiActions()
}