package com.briefy.screen.location.view.module_items

import android.widget.TextView
import com.briefy.R
import com.briefy.screen.location.view.models.AutoCompleteLocationUiModel
import com.idanatz.oneadapter.external.event_hooks.ClickEventHook
import com.idanatz.oneadapter.external.modules.ItemModule

class AutoCompleteLocationItem(onItemClicked: (AutoCompleteLocationUiModel) -> Unit) : ItemModule<AutoCompleteLocationUiModel>() {
	init {
		config {
			layoutResource = R.layout.item_auto_complete_location
		}
		onBind { model, viewBinder, _ ->
			val text = viewBinder.rootView.context.getString(R.string.location_city_and_country).format(model.cityName, model.countryName)
			viewBinder.findViewById<TextView>(R.id.textView).text = text
		}
		eventHooks += ClickEventHook<AutoCompleteLocationUiModel>().apply {
			onClick { model, _, _ -> onItemClicked(model) }
		}
	}
}