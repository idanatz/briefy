package com.briefy.screen.location.view.models

import com.idanatz.oneadapter.external.interfaces.Diffable

data class Label(val label: String) : Diffable {

	override val uniqueIdentifier: Long = hashCode().toLong()

	override fun areContentTheSame(other: Any): Boolean = other is Label && other == this
}