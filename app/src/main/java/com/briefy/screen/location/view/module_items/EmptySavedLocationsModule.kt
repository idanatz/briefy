package com.briefy.screen.location.view.module_items

import com.briefy.R
import com.idanatz.oneadapter.external.modules.EmptinessModule

class EmptySavedLocationsModule : EmptinessModule() {
	init {
		config {
			layoutResource = R.layout.item_no_locations
		}
	}
}