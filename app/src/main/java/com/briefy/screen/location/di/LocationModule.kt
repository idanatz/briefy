package com.briefy.screen.location.di

import com.briefy.screen.location.presentation.LocationFragmentViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val locationScreenModule = module {

	viewModel { LocationFragmentViewModel() }
}