package com.briefy.screen.location.view.module_items

import android.widget.ImageView
import android.widget.TextView
import com.briefy.R
import com.briefy.screen.location.view.models.SavedLocationUiModel
import com.idanatz.oneadapter.external.modules.ItemModule

class SavedLocationItem(onRemoveClicked: (SavedLocationUiModel) -> Unit) : ItemModule<SavedLocationUiModel>() {
	init {
		config {
			layoutResource = R.layout.item_saved_location
		}
		onBind { model, viewBinder, _ ->
			val text = viewBinder.rootView.context.getString(R.string.location_city_and_country).format(model.cityName, model.countryName)
			viewBinder.findViewById<TextView>(R.id.textView).text = text

			val removeButton = viewBinder.findViewById<ImageView>(R.id.remove_button)
			removeButton.setOnClickListener { onRemoveClicked(model) }
		}
	}
}