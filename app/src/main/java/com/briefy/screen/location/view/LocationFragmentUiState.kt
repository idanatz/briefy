package com.briefy.screen.location.view

import com.briefy.model.UiState
import com.briefy.screen.location.view.models.AutoCompleteLocationUiModel
import com.briefy.screen.location.view.models.SavedLocationUiModel

sealed class LocationFragmentUiStates {

	data class Main(
			val userSavedLocations: UiState<List<SavedLocationUiModel>> = UiState.Init
	) : LocationFragmentUiStates()

	data class SearchingLocation(
			val autoCompleteLocations: UiState<List<AutoCompleteLocationUiModel>> = UiState.Init
	) : LocationFragmentUiStates()
}