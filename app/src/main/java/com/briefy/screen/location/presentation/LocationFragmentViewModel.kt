package com.briefy.screen.location.presentation

import androidx.lifecycle.viewModelScope
import com.briefy.PhoneLocationManager
import com.briefy.mappers.LocationMapper.Companion.mapAddressAndCityToLocation
import com.briefy.model.*
import com.briefy.model.ActionResult.Data as DataResult
import com.briefy.model.ActionResult.Success as SuccessResult
import com.briefy.model.ActionResult.Empty as EmptyResult
import com.briefy.model.ActionResult.Error as ErrorResult
import com.briefy.repository.CityRepository
import com.briefy.repository.Repository
import com.briefy.usecases.framework.GetLocation
import com.briefy.usecases.local.UpdateUser
import com.briefy.usecases.local.UserLocalActions
import com.briefy.usecases.remote.GetCity
import com.briefy.screen.location.view.LocationFragmentUiActions
import com.briefy.screen.location.view.LocationFragmentUiStates
import com.briefy.screen.location.view.LocationFragmentUiStates.*
import com.briefy.screen.location.view.models.AutoCompleteLocationUiModel
import com.briefy.screen.location.view.models.SavedLocationUiModel
import com.briefy.viewmodel.base.UiStateManager
import com.briefy.viewmodel.base.UiStateViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

private const val MIN_QUERY_LENGTH = 4

class LocationFragmentViewModel : UiStateViewModel<LocationFragmentUiStates, LocationFragmentUiActions>(), KoinComponent {

    private val phoneLocationManager: PhoneLocationManager by inject()
	private val userRepository: Repository by inject()
	private val locationRepository: Repository by inject()
	private val cityRepository: CityRepository by inject()

	override val uiStateManager: UiStateManager<LocationFragmentUiStates> = UiStateManager(
			Main::class.java to Main(),
			SearchingLocation::class.java to SearchingLocation()
	)

	private var autoCompleteLocations = mutableSetOf<Location>()

	init {
		viewModelScope.launch {
			fetchUserSavedLocations()
		}
    }

    fun autoCompleteTextChanged(text: String) {
        when  {
            text.isEmpty() -> {
                autoCompleteLocations.clear()
				uiStateManager.set<Main>()
            }
            text.length >= MIN_QUERY_LENGTH -> {
				viewModelScope.launch {
					queryLocationAndCompareItToUserLocations(text)
				}
			}
        }
    }

    fun autoCompleteResultClicked(locationId: Int) {
        autoCompleteLocations.find { it.locationId == locationId }?.let {
			viewModelScope.launch {
				updateUserWithPickedLocation(it)
			}
        }
    }

	fun removeSavedLocationClicked(locationId: Int) {
		viewModelScope.launch {
			updateUserWithRemovedLocation(locationId)
		}
	}

    fun useMyLocationClicked() {
		viewModelScope.launch {
			when (val locationResponse = phoneLocationManager.getCurrentLocation()) {
				is DataResult -> {
					when (val cityResponse = cityRepository.perform(GetCity(), GetCity.Params(locationResponse.data.locality))) {
						is DataResult -> {
							val location = mapAddressAndCityToLocation(locationResponse.data, cityResponse.data)
							updateUserWithPickedLocation(location)
						}
						is ErrorResult -> uiActionObservable.accept(LocationFragmentUiActions.ShowErrorMessage(cityResponse.info))
						else -> {} // todo handle errors
					}
				}
				is ErrorResult -> uiActionObservable.accept(LocationFragmentUiActions.ShowErrorMessage(locationResponse.info))
				else -> {} // todo handle errors
			}
		}
    }

    private suspend fun updateUserWithPickedLocation(newLocation: Location) {
		when (val userResponse = userRepository.perform(UserLocalActions.GetUser())) {
			is DataResult -> {
				val user = userResponse.data
				when (user.savedLocations.contains(newLocation)) {
					false -> {
						user.savedLocations += newLocation

						when (val updateResponse = userRepository.perform(UpdateUser(), UpdateUser.Params(user))) {
							SuccessResult -> uiActionObservable.accept(LocationFragmentUiActions.ShowSuccessMessage("Location added"))
							is ErrorResult -> uiActionObservable.accept(LocationFragmentUiActions.ShowErrorMessage(updateResponse.info))
						}
					}
					true -> uiActionObservable.accept(LocationFragmentUiActions.ShowWarningMessage("Location already saved"))
				}
			}
			is ErrorResult -> uiActionObservable.accept(LocationFragmentUiActions.ShowErrorMessage(userResponse.info))
		}
    }

	private suspend fun updateUserWithRemovedLocation(locationId: Int) {
		when (val userResponse = userRepository.perform(UserLocalActions.GetUser())) {
			is DataResult -> {
				val user = userResponse.data
				user.savedLocations = user.savedLocations.filterNot { it.locationId == locationId }.toSet()

				when (val updateResponse = userRepository.perform(UpdateUser(), UpdateUser.Params(user))) {
					SuccessResult -> uiActionObservable.accept(LocationFragmentUiActions.ShowSuccessMessage("Location removed"))
					is ErrorResult -> uiActionObservable.accept(LocationFragmentUiActions.ShowErrorMessage(updateResponse.info))
				}
			}
			is ErrorResult -> uiActionObservable.accept(LocationFragmentUiActions.ShowErrorMessage(userResponse.info))
		}
	}

    private suspend fun fetchUserSavedLocations() {
		uiStateManager.set<Main> { copy(userSavedLocations = UiState.Loading(true)) }

		userRepository.perform(UserLocalActions.ObserveUser())
				.onStart {
					uiStateManager.set<Main> { copy(userSavedLocations = UiState.Loading(false)) }
				}
				.onEach { userResponse ->
					val state = when (userResponse) {
						is DataResult -> {
							val locationUiModels = userResponse.data.savedLocations
							when {
								locationUiModels.isEmpty() -> UiState.Empty
								else -> UiState.Data(locationUiModels.map { it.toSavedLocationUiModel() }.toList())
							}
						}
						is ActionResult.Error -> UiState.Error(userResponse.info)
						is EmptyResult -> UiState.Empty
						else -> UiState.Error("something went wrong in user state")
					}

					uiStateManager.set<Main> { copy(userSavedLocations = state) }
				}
				.launchIn(viewModelScope)
    }

    private suspend fun queryLocationAndCompareItToUserLocations(text: String) {
		uiStateManager.set<SearchingLocation> { copy(autoCompleteLocations = UiState.Loading(true)) }

		val state = when (val locationResponse = locationRepository.perform(GetLocation(), GetLocation.Params(text))) {
			is DataResult -> {
				val locations = locationResponse.data.mapNotNull { location ->
					when (val cityResponse = cityRepository.perform(GetCity(), GetCity.Params(location.locality))) {
						is DataResult -> mapAddressAndCityToLocation(location, cityResponse.data)
						else -> null // @TODO: 22/11/2020 log?
					}
				}

				when {
					locations.isEmpty() -> UiState.Empty
					else -> {
						autoCompleteLocations.clear()
						autoCompleteLocations.addAll(locations)
						UiState.Data(locations.map { it.toAutoCompleteLocationUiModel() }.toList())
					}
				}
			}
			is EmptyResult -> UiState.Empty
			else -> UiState.Error("something went wrong in locationResponse")
		}

		uiStateManager
				.set<SearchingLocation> { copy(autoCompleteLocations = UiState.Loading(false)) }
				.set<SearchingLocation> { copy(autoCompleteLocations = state) }
    }

	private fun Location.toSavedLocationUiModel(): SavedLocationUiModel {
		return SavedLocationUiModel(
				id = locationId,
				cityName = city.cityName,
				countryName = country
		)
	}

	private fun Location.toAutoCompleteLocationUiModel(): AutoCompleteLocationUiModel {
		return AutoCompleteLocationUiModel(
				id = locationId,
				cityName = city.cityName,
				countryName = country
		)
	}
}