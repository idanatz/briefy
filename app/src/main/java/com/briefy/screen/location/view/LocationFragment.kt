package com.briefy.screen.location.view

import android.graphics.PorterDuff
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.briefy.R
import com.briefy.ThemeEngine
import com.briefy.databinding.FragmentLocationBinding
import com.briefy.model.UiState.*
import com.briefy.utils.*
import com.briefy.utils.ui.*
import com.briefy.view.base.BaseFragment
import com.briefy.screen.location.view.LocationFragmentUiActions.*
import com.briefy.screen.location.view.models.Label
import com.briefy.screen.location.view.module_items.LabelHeaderItem
import com.briefy.screen.location.view.module_items.SavedLocationItem
import com.briefy.screen.location.view.LocationFragmentUiStates.*
import com.briefy.screen.location.view.module_items.AutoCompleteLocationItem
import com.briefy.screen.location.view.module_items.EmptySavedLocationsModule
import com.briefy.screen.location.presentation.LocationFragmentViewModel
import com.idanatz.oneadapter.OneAdapter
import com.idanatz.oneadapter.external.interfaces.Diffable
import kotlinx.coroutines.flow.*
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener
import org.koin.android.ext.android.inject

class LocationFragment : BaseFragment(R.layout.fragment_location) {

	private val binding by viewBinding(FragmentLocationBinding::bind)

	private val viewModel: LocationFragmentViewModel by viewModels()
	private val themeEngine: ThemeEngine by inject()

	private var oneAdapter: OneAdapter? = null

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

        initLocationsAutoComplete()
        initLocationsList()
        initUseMyLocation()
        attachClickListeners()

        observeViewModel()
        observeTheme()
    }

    private fun initLocationsAutoComplete() {
		lifecycleScope.launchWhenStarted {
			binding.locationsSearchBox.textChangesFlow
					.map { it.trim().toString() }
					.debounce(500)
					.collectLatest { viewModel.autoCompleteTextChanged(it) }
		}
    }

    private fun initLocationsList() {
		binding.locationsRecyclerView.let {
			oneAdapter = OneAdapter(it) {
				itemModules += LabelHeaderItem()
				itemModules += SavedLocationItem(
						onRemoveClicked = { model -> viewModel.removeSavedLocationClicked(model.id) }
				)
				itemModules += AutoCompleteLocationItem(
						onItemClicked = { model -> viewModel.autoCompleteResultClicked(model.id) }
				)
				emptinessModule = EmptySavedLocationsModule()
			}
		}
    }

    private fun initUseMyLocation() {
		binding.useMyLocationButton.run { post { tag = height } } // save the original height for later animation
		KeyboardVisibilityEvent.setEventListener(activity as AppCompatActivity, viewLifecycleOwner, object : KeyboardVisibilityEventListener {
			override fun onVisibilityChanged(isOpen: Boolean) {
				animateUseMyLocation(isOpen)
			}
		})
	}

    private fun animateUseMyLocation(visible: Boolean) {
		binding.useMyLocationButton.run {
            if (visible) animateShrinkHeight()
            else animateExpandHeight(tag as Int)
        }
    }

    private fun attachClickListeners() {
        binding.navDrawerButton.setOnClickListener { activity?.openDrawer() }
		binding.useMyLocationButton.setOnClickListener { viewModel.useMyLocationClicked() }
    }

    private fun observeViewModel() {
		viewModel.run {
			uiStateObservable.subscribe { state ->
				when (state) {
					is Main -> {
						when (val savedLocationState = state.userSavedLocations) {
							is Loading -> binding.progressBar.visibility = savedLocationState.isLoading.toVisibility()
							Empty -> oneAdapter?.setItems(listOf())
							is Data -> {
								setHeaderAndLocations("Saved Locations", savedLocationState.data)
								binding.locationsSearchBox.text?.clear()
							}
							is Error -> Toast.makeText(activity, savedLocationState.reason, Toast.LENGTH_LONG).show()
						}
					}
					is SearchingLocation -> {
						when (val autoCompleteLocationsState = state.autoCompleteLocations) {
							is Loading -> binding.progressBar.visibility = autoCompleteLocationsState.isLoading.toVisibility()
							is Data -> setHeaderAndLocations("Found Locations", autoCompleteLocationsState.data)
							is Error -> Toast.makeText(activity, autoCompleteLocationsState.reason, Toast.LENGTH_LONG).show()
						}
					}
				}
			}
			.disposeBy(compositeDisposable)

			uiActionObservable.subscribe { action ->
				when (action) {
					is ShowSuccessMessage -> Toast.makeText(activity, action.msg, Toast.LENGTH_SHORT).show()
					is ShowWarningMessage -> Toast.makeText(activity, action.msg, Toast.LENGTH_SHORT).show()
					is ShowErrorMessage -> Toast.makeText(activity, action.msg, Toast.LENGTH_SHORT).show()
				}
			}.disposeBy(compositeDisposable)
		}
    }

    private fun setHeaderAndLocations(header: String, locations: List<Diffable>) {
		val items = listOf(Label(header)) + locations
		oneAdapter?.setItems(items)
    }

    private fun observeTheme() {
        themeEngine.run {
            themeRelay.distinctUntilChanged().subscribe { theme ->
                binding.toolbar.background = ColorDrawable(theme.statusBarColor)
				binding.progressBar.indeterminateDrawable?.setColorFilter(theme.mainColor, PorterDuff.Mode.SRC_IN)
            }
            .disposeBy(compositeDisposable)
        }
    }
}