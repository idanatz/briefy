package com.briefy

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import com.briefy.model.WeatherInfo
import com.briefy.utils.getColorCompat
import com.briefy.utils.getDrawableCompat
import com.jakewharton.rxrelay2.BehaviorRelay
import com.mikepenz.iconics.IconicsDrawable
import com.mikepenz.weather_icons_typeface_library.WeatherIcons

class ThemeEngine(private val context: Context) {

	val themeRelay: BehaviorRelay<Theme> = BehaviorRelay.createDefault(Theme(context.getColorCompat(R.color.white), context.getColorCompat(R.color.white), context.getColorCompat(R.color.white)))

    fun init(weatherType: WeatherInfo.Type, isDayTime: Boolean) {
        val backgroundDrawable = context.getDrawableCompat(loadWeatherBackground(weatherType, isDayTime))
        val (startColor, endColor) = getThemeColors(weatherType, isDayTime)

        themeRelay.accept(Theme(
                statusBarColor = (backgroundDrawable as BitmapDrawable).bitmap.getPixel(0, 0),
                mainColor = Color.parseColor(endColor),
                subColor = Color.parseColor(startColor),
				weatherBackground = backgroundDrawable
        ))
    }

    private fun getThemeColors(weatherType: WeatherInfo.Type, isDayTime: Boolean = false) =
        when (weatherType) {
            WeatherInfo.Type.Clear -> if (isDayTime) Pair("#CCFEC092", "#CCFF4D48") else Pair("#CC4D5374", "#CC031042")
            WeatherInfo.Type.Clouds -> if (isDayTime) Pair("#CCB9D9FE", "#CC459AFF") else Pair("#CC2E7C8C", "#CC012F3B")
            WeatherInfo.Type.Windy -> Pair("#CCC153E6", "#CC2D0E65")
            WeatherInfo.Type.Rain -> Pair("#CC939DD9", "#CC331FF8")
            WeatherInfo.Type.Thunder -> Pair("#CC645384", "#CC150E48")
            WeatherInfo.Type.Snow -> Pair("#CC8EE1FF", "#CC1D86FF")
            WeatherInfo.Type.Unknown -> Pair("White", "White")
        }

    fun getWeatherIcon(weatherType: WeatherInfo.Type, isAfterSunset: Boolean = false, color: Int = 0): IconicsDrawable {
        val weatherIcon = when (weatherType) {
            WeatherInfo.Type.Clear -> if (isAfterSunset) WeatherIcons.Icon.wic_night_clear else WeatherIcons.Icon.wic_day_sunny
            WeatherInfo.Type.Clouds -> if (isAfterSunset) WeatherIcons.Icon.wic_night_alt_cloudy else WeatherIcons.Icon.wic_day_cloudy
            WeatherInfo.Type.Windy -> if (isAfterSunset) WeatherIcons.Icon.wic_night_alt_cloudy_gusts else WeatherIcons.Icon.wic_day_cloudy_gusts
            WeatherInfo.Type.Rain -> if (isAfterSunset) WeatherIcons.Icon.wic_night_alt_rain else WeatherIcons.Icon.wic_day_rain
            WeatherInfo.Type.Thunder -> if (isAfterSunset) WeatherIcons.Icon.wic_night_alt_thunderstorm else WeatherIcons.Icon.wic_day_thunderstorm
            WeatherInfo.Type.Snow -> if (isAfterSunset) WeatherIcons.Icon.wic_night_alt_snow else WeatherIcons.Icon.wic_day_snow
            WeatherInfo.Type.Unknown -> WeatherIcons.Icon.wic_na
        }

        return if (color == 0) IconicsDrawable(context, weatherIcon).color(themeRelay.value.mainColor)
        else getIcon(weatherIcon, color)
    }

    fun getIcon(icon: WeatherIcons.Icon, color: Int): IconicsDrawable = IconicsDrawable(context, icon).color(context.getColorCompat(color))

    private fun loadWeatherBackground(weatherType: WeatherInfo.Type, isDayTime: Boolean): Int =
            when (weatherType) {
                WeatherInfo.Type.Clear -> if (isDayTime) R.drawable.clear_day_background else R.drawable.clear_night_background
                WeatherInfo.Type.Clouds -> if (isDayTime) R.drawable.foggy_background else R.drawable.hazy_background
                WeatherInfo.Type.Windy -> R.drawable.windy_background
                WeatherInfo.Type.Rain -> R.drawable.rainy_background
                WeatherInfo.Type.Thunder -> R.drawable.thunder_background
                WeatherInfo.Type.Snow -> R.drawable.snow_background
                WeatherInfo.Type.Unknown -> R.color.white //TODO: finish this
            }

//    fun getHourIcon(timestamp: Long): IconicsDrawable {
//        val formatter = SimpleDateFormat("HH:mm", Locale.getDefault())
//        val hourIcon = when (formatter.format(timestamp)) {
//            "00:00", "12:00" -> WeatherIcons.Icon.wic_time_12
//            "03:00", "15:00" -> WeatherIcons.Icon.wic_time_3
//            "06:00", "18:00" -> WeatherIcons.Icon.wic_time_6
//            "09:00", "21:00" -> WeatherIcons.Icon.wic_time_9
//            else -> WeatherIcons.Icon.wic_na
//        }
//        return IconicsDrawable(context, hourIcon).color(ContextCompat.getColor(context, R.color.grey_600))
//    }
}

data class Theme(
        val mainColor: Int,
        val subColor: Int,
        val statusBarColor: Int,
		val weatherBackground: Drawable? = null
) {
    val gradient: GradientDrawable = GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, intArrayOf(subColor, mainColor))
}