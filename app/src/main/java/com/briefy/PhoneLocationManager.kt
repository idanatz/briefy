package com.briefy

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.annotation.SuppressLint
import android.content.Context
import android.location.Address
import android.location.Location
import com.briefy.location.CoGeocoder
import com.briefy.model.AndroidLocation
import com.briefy.model.ActionResult
import com.briefy.model.ActionResult.Data as DataResult
import com.briefy.model.ActionResult.Error as ErrorResult
import com.briefy.utils.isPermissionGranted
import com.google.android.gms.location.LocationRequest
import com.briefy.location.CoLocation
import kotlinx.coroutines.flow.*

class PhoneLocationManager(
        private val context: Context,
		private val locationProvider: CoLocation,
		private val geocoderProvider: CoGeocoder
    ) {

    suspend fun getLastAndCurrentAddress(): Flow<ActionResult<AndroidLocation>> {
        return if (context.isPermissionGranted(ACCESS_FINE_LOCATION)) {
			getCurrentLocationUpdates()
					.onStart { getLastLocation() }
        } else {
			flowOf(ErrorResult.LocationPermissionDenied()) //TODO: react to this error
        }
    }

    @SuppressLint("MissingPermission")
     private suspend fun getLastLocation(): ActionResult<AndroidLocation> {
		return when (val location = locationProvider.getLastLocation()) {
			null -> ErrorResult.LocationNotFound("current location was null")
			else -> mapLocationToAddressState(location)
		}
    }

    suspend fun getCurrentLocation(): ActionResult<AndroidLocation> {
        return getLocationUpdates(numberOfUpdates = 1).first()
    }

	fun getCurrentLocationUpdates(): Flow<ActionResult<AndroidLocation>> {
		return getLocationUpdates()
	}

	@SuppressLint("MissingPermission")
	private fun getLocationUpdates(numberOfUpdates: Int = -1): Flow<ActionResult<AndroidLocation>> {
		val locationRequest = LocationRequest.create().apply {
			priority = LocationRequest.PRIORITY_HIGH_ACCURACY
			if (numberOfUpdates != -1) {
				numUpdates = numberOfUpdates
			}
		}

		return locationProvider.getLocationUpdates(locationRequest)
				.map { location -> mapLocationToAddressState(location) }
	}

	private suspend fun mapLocationToAddressState(location: Location): ActionResult<Address> {
		return when (val androidLocation = geocoderProvider.getAddressFromLocation(location.latitude, location.longitude)) {
			null -> ErrorResult.LocationNotFound("getAddressFromLocation was null")
			else -> DataResult(androidLocation)
		}
	}
}