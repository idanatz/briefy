package com.briefy.network

import okhttp3.OkHttpClient
import retrofit2.Converter
import retrofit2.Retrofit

class RetrofitClient(okHttpClient: OkHttpClient, converterFactory: Converter.Factory) {

    private val retrofit: Retrofit = Retrofit.Builder().apply {
        baseUrl("http://api.openweathermap.org/data/2.5/")
        addConverterFactory(converterFactory)
        client(okHttpClient)
    }.build()

    fun <T> create(service: Class<T>): T = retrofit.create(service)
}