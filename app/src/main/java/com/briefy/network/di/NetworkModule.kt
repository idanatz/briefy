@file:Suppress("RemoveExplicitTypeArguments")

package com.briefy.network.di

import com.briefy.network.HttpClient
import com.briefy.network.RetrofitClient
import okhttp3.OkHttpClient
import org.koin.core.module.Module
import org.koin.dsl.module
import retrofit2.Converter
import retrofit2.converter.gson.GsonConverterFactory

val networkModule : Module = module {

	factory<Converter.Factory> { GsonConverterFactory.create() as Converter.Factory}

	factory<OkHttpClient> { HttpClient.create() }

	single { RetrofitClient(okHttpClient = get(), converterFactory = get()) }
}