package com.briefy.network

import okhttp3.OkHttpClient

class HttpClient {

	companion object {

		fun create(): OkHttpClient {
			val httpClient = OkHttpClient.Builder()

			// Request customization: add request fixed query parameters
			httpClient.addInterceptor { chain ->
				val original = chain.request()
				val newUrl = original.url().newBuilder()
						.addQueryParameter("units", "metric")
						.addQueryParameter("APPID", "ee2f388f24fde4588fb49e623f8a5b58")
						.build()
				val requestBuilder = original.newBuilder().url(newUrl)
				val request = requestBuilder.build()
				chain.proceed(request)
			}
			return httpClient.build()
		}
	}
}