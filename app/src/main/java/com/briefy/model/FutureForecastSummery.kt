package com.briefy.model

/**
 * Created by Idan Atsmon on 31/05/2018.
 */
class FutureForecastSummery {

    var lowestMinTemperature: Double? = null
    var highestMaxTemperature: Double? = null
    private val weatherInfoArray: IntArray = IntArray(WeatherInfo.Type.values().size)

    val averageWeatherInfo: WeatherInfo.Type?
        get() = weatherInfoArray.withIndex().maxBy {
            it.value
        }?.let {
            WeatherInfo.Type.values()[it.index]
        }

    fun addTemperature(minTemperature: Double, maxTemperature: Double) {
        lowestMinTemperature = lowestMinTemperature?.let {
            if (it > minTemperature) minTemperature else it
        } ?: minTemperature

        highestMaxTemperature = highestMaxTemperature?.let {
            if (it > maxTemperature) maxTemperature else it
        } ?: maxTemperature
    }

    fun addWeatherInfoType(weatherInfoType: WeatherInfo.Type) {
        weatherInfoArray[weatherInfoType.ordinal]++
    }
}