package com.briefy.model

class FullDayForecastSummery {

    var forecastsCount: Int = 0
    private var temperatureSum: Double = 0.0
    private val weatherInfoArray: IntArray = IntArray(WeatherInfo.Type.values().size)

    val averageTemperature: Double?
        get() = if (forecastsCount != 0 ) temperatureSum / forecastsCount else null

    val averageWeatherInfo: WeatherInfo.Type?
        get() = weatherInfoArray.withIndex().maxByOrNull { it.value }?.let { WeatherInfo.Type.values()[it.index] }

    fun addTemperature(temperature: Double) {
        temperatureSum += temperature
        forecastsCount++
    }

    fun addWeatherInfoType(weatherInfoType: WeatherInfo.Type) {
        weatherInfoArray[weatherInfoType.ordinal]++
    }
}