package com.briefy.model

import androidx.room.Embedded
import android.location.Address

typealias AndroidLocation = Address

data class Location(
		val country: String,
		@Embedded val city: City
) {

	val locationId
		get() = city.cityId + country.hashCode()

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (javaClass != other?.javaClass) return false

		other as Location
		return locationId == other.locationId
	}

	override fun hashCode(): Int {
		return locationId
	}
}