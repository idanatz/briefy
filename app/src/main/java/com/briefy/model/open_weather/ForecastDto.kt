package com.briefy.model.open_weather

import com.google.gson.annotations.SerializedName

data class ForecastDto(

		@SerializedName("city") val city: CityDto? = null,
		@SerializedName("cnt") val cnt: Int? = null,
		@SerializedName("cod") val cod: String? = null,
		@SerializedName("message") val message: Double? = null,
		@SerializedName("list") val list: List<WeatherDto>? = null
)