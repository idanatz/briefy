package com.briefy.model.open_weather

import com.google.gson.annotations.SerializedName

data class CityDto (

		@SerializedName("country") val country: String? = null,
		@SerializedName("coord") val coord: CoordDto? = null,
		@SerializedName("name") val name: String? = null,
		@SerializedName("id") val id: Int? = null,
		@SerializedName("population") val population: Int? = null
)