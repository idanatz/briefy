package com.briefy.model.open_weather

import com.google.gson.annotations.SerializedName

data class WeatherDto(

		@SerializedName("cod") val cod: String? = null,
		@SerializedName("coord") val coord: CoordDto? = null,
		@SerializedName("visibility") val visibility: Int? = null,
		@SerializedName("weather") val weather: List<WeatherItemDto?>? = null,
		@SerializedName("name") val name: String? = null,
		@SerializedName("main") val main: MainDto? = null,
		@SerializedName("clouds") val clouds: CloudsDto? = null,
		@SerializedName("id") val id: Int? = null,
		@SerializedName("sys") val sys: SysDto? = null,
		@SerializedName("base") val base: String? = null,
		@SerializedName("wind") val wind: WindDto? = null,

		// time vars
		@SerializedName("dt") val dt: Int? = null,
		@SerializedName("dt_txt") val dtTxt: String? = null
)