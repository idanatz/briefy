package com.briefy.model.open_weather

import com.google.gson.annotations.SerializedName

data class CloudsDto(

    @SerializedName("all") val all: Int? = null
)

data class CoordDto(

    @SerializedName("lon") var lon: Double? = null,
    @SerializedName("lat") var lat: Double? = null
)

data class MainDto(

    @SerializedName("temp") val temp: Double? = null,
    @SerializedName("temp_min") val tempMin: Double? = null,
    @SerializedName("humidity") val humidity: Int? = null,
    @SerializedName("pressure") val pressure: Double? = null,
    @SerializedName("temp_max") val tempMax: Double? = null
)

data class SysDto(

    @SerializedName("country") val country: String? = null,
    @SerializedName("sunrise") val sunrise: Int? = null,
    @SerializedName("sunset") val sunset: Int? = null,
    @SerializedName("id") val id: Int? = null,
    @SerializedName("type") val type: Int? = null,
    @SerializedName("message") val message: Double? = null
)

data class WeatherItemDto(

    @SerializedName("icon") val icon: String? = null,
    @SerializedName("description") val description: String? = null,
    @SerializedName("main") val main: String? = null,
    @SerializedName("id") val id: Int? = null
)

data class WindDto(

    @SerializedName("deg") val deg: Double? = null,
    @SerializedName("speed") val speed: Double? = null,
    @SerializedName("gust") val gust: Double? = null
)