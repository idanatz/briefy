package com.briefy.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "forecast")
data class Forecast(
        @PrimaryKey val id: Int,
        @Embedded val city: City,
        @Embedded val temperature: Temperature,
        @Embedded val weatherInfo: WeatherInfo,
        val calendar: Calendar
)

data class Temperature(
    val minTemp: Double,
    val maxTemp: Double
) {
    val averageTemp: Double get() = (minTemp + maxTemp) / 2
}