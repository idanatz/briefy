package com.briefy.model

import java.util.*

data class City(
        val cityId: Int,
        val cityName: String,
        val sunriseCalendar: Calendar? = null,
        val sunsetCalendar: Calendar? = null
)