package com.briefy.model

/**
 * Created by Idan Atsmon on 01/03/2018.
 */

data class WeatherInfo(
        val type: Type,
        val mainDescription: String,
        val subDescription: String
) {

    enum class Type {
        Clear, Clouds, Windy, Rain, Thunder, Snow, Unknown;
    }
}