package com.briefy.model

sealed class UiState<out T : Any> {

	object Init : UiState<Nothing>()

	object Empty : UiState<Nothing>()

	data class Loading(val isLoading: Boolean) : UiState<Nothing>()

	data class Data<out T : Any>(val data: T) : UiState<T>()

	data class Error(val reason: String) : UiState<Nothing>()
}