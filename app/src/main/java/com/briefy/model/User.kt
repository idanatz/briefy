package com.briefy.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user")
data class User(
        @PrimaryKey val id: Int = 0,
        @Embedded var currentLocation: Location? = null,
        var savedLocations: Set<Location> = emptySet()
)