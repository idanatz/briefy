package com.briefy.model

sealed class ActionResult<out T : Any> {

    object Success : ActionResult<Nothing>()

    object Empty : ActionResult<Nothing>()

	class Data<out T : Any>(val data: T) : ActionResult<T>()

    sealed class Error(val info: String) : ActionResult<Nothing>() {
		class LocationNotFound(info: String = "") : Error(info)
		class LocationPermissionDenied(info: String = "") : Error(info)

		sealed class Network(info: String) : Error(info) {
			class ResourceRemoved(info: String = "") : Network(info)
			class RemovedResourceFound(info: String = "") : Network(info)
			class ResourceForbidden(info: String = "") : Network(info)
			class ResourceNotFound(info: String = "") : Network(info)
			class InternalServerError(info: String = "") : Network(info)
			class TooManyRequests(info: String = "") : Network(info)
			class BadGateWay(info: String = "") : Network(info)
			class Unknown(info: String = "") : Network(info)
		}

		class DataNotFound(info: String = "") : Error(info)
		class Unknown(info: String = "") : Error(info)
		override fun toString(): String = "${javaClass.simpleName}: $info"
    }
}