package com.briefy.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.briefy.PhoneLocationManager
import com.briefy.mappers.LocationMapper.Companion.mapAddressAndCityToLocation
import com.briefy.model.*
import com.briefy.view.home.models.CurrentForecastUiModel
import com.briefy.view.home.models.FutureForecastsUiModel
import com.briefy.usecases.local.UserLocalActions
import com.jakewharton.rxrelay2.BehaviorRelay
import java.util.*
import com.briefy.model.ActionResult.Empty as EmptyResult
import com.briefy.model.ActionResult.Data as DataResult
import com.briefy.model.ActionResult.Error as ErrorResult
import com.briefy.repository.CityRepository
import com.briefy.repository.ForecastRepository
import com.briefy.repository.Repository
import com.briefy.usecases.local.ForecastLocalActions
import com.briefy.usecases.local.InsertUser
import com.briefy.usecases.local.UpdateUser
import com.briefy.usecases.remote.GetCity
import com.briefy.usecases.remote.GetCurrentForecast
import com.briefy.usecases.remote.GetThreeDaysForecast
import com.briefy.utils.let2
import com.briefy.utils.toCamelCase
import com.briefy.view.home.models.CurrentLocationUiModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import kotlin.math.roundToInt

class HomeFragmentViewModel : ViewModel(), KoinComponent {

	private val phoneLocationManager: PhoneLocationManager by inject()

	private val userRepository: Repository by inject()
	private val forecastRepository: ForecastRepository by inject()
	private val cityRepository: CityRepository by inject()

	val currentLocationRelay: BehaviorRelay<UiState<CurrentLocationUiModel>> = BehaviorRelay.createDefault(UiState.Init)
	val currentForecastRelay: BehaviorRelay<UiState<CurrentForecastUiModel>> = BehaviorRelay.createDefault(UiState.Init)
	val futureForecastRelay: BehaviorRelay<UiState<FutureForecastsUiModel>> = BehaviorRelay.createDefault(UiState.Init)

	init {
		// TODO: load the last state of the app for quick loading and then start loading new data, old state is location, curr and future forecast. the load should be with room single, and them start to observe the user with flowable
		viewModelScope.launch {
			createUserIfNeeded()
			observeUserChanges()
		}
	}

	fun onLocationPermissionGranted() {
		viewModelScope.launch {
			fetchAndUpdateLocation()
		}
	}

	private suspend fun createUserIfNeeded() {
		// @TODO: 03/11/2020 do somethings with the return value
		when (userRepository.perform(UserLocalActions.GetUser())) {
			is EmptyResult -> userRepository.perform(InsertUser(), InsertUser.Params(User()))
		}
	}

	private suspend fun observeUserChanges() {
		currentForecastRelay.accept(UiState.Loading(true))
		futureForecastRelay.accept(UiState.Loading(true))

		userRepository.perform(UserLocalActions.ObserveUser())
				.onStart {
					currentForecastRelay.accept(UiState.Loading(false))
					futureForecastRelay.accept(UiState.Loading(false))
				}
				.onEach { userResponse ->
					when (userResponse) {
						is DataResult -> {
							userResponse.data.currentLocation?.let { userCurrentLocation -> // if i have current location continue
								// fetch forecasts & insert them to repository
								val currentForecastResponse = forecastRepository.perform(GetCurrentForecast(), GetCurrentForecast.Params(userCurrentLocation.city.cityName))
								val threeDaysForecastResponse = forecastRepository.perform(GetThreeDaysForecast(), GetThreeDaysForecast.Params(userCurrentLocation.city.cityName, false))

								when (threeDaysForecastResponse) {
									is DataResult -> persistForecasts(threeDaysForecastResponse.data)
								}

								val currentForecastUiModel = when (currentForecastResponse) {
									is DataResult -> UiState.Data(createCurrentForecastUiModel(currentForecastResponse))
									is ErrorResult -> UiState.Error(currentForecastResponse.info)
									else -> UiState.Error("something went wrong in current forecast state")
								}

								val futureForecastsUiModel = when (threeDaysForecastResponse) {
									is DataResult -> UiState.Data(createFutureForecastsUiModel(threeDaysForecastResponse))
									is ErrorResult -> UiState.Error(threeDaysForecastResponse.info)
									else -> UiState.Error("something went wrong in future forecast state")
								}

								val locationUiModel = UiState.Data(createLocationUiModel(userCurrentLocation))

								currentForecastRelay.accept(currentForecastUiModel)
								futureForecastRelay.accept(futureForecastsUiModel)
								currentLocationRelay.accept(locationUiModel)
							}
						}
						is ErrorResult -> {}
						else -> { }
					}
				}
				.launchIn(viewModelScope)
	}

	private suspend fun persistForecasts(threeDaysForecasts: List<Forecast>) {
		// @TODO: 03/11/2020 do somethings with the return value
		forecastRepository.perform(ForecastLocalActions.InsertForecasts(threeDaysForecasts))
	}

	private fun createFutureForecastsUiModel(futureForecastState: DataResult<List<Forecast>>): FutureForecastsUiModel {
		val futureForecastsUiModel = FutureForecastsUiModel(mutableListOf())
		val todayDayOfTheMonth = Calendar.getInstance()[Calendar.DAY_OF_MONTH]

		futureForecastState.data
				.groupBy { it.calendar[Calendar.DAY_OF_MONTH] }
				.filterKeys { it != todayDayOfTheMonth }
				.toSortedMap { o1, o2 -> o1.compareTo(o2) } // order by ascending date
				.values
				.forEach { dayOfForecasts ->
					val futureForecastSummery = FutureForecastSummery()

					dayOfForecasts.forEach { forecast ->
						futureForecastSummery.addTemperature(forecast.temperature.minTemp, forecast.temperature.maxTemp)
						futureForecastSummery.addWeatherInfoType(forecast.weatherInfo.type)
					}

					futureForecastsUiModel.forecasts.add(FutureForecastsUiModel.FutureForecastUiModel(
							dayOfTheWeek = dayOfForecasts.firstOrNull()?.calendar?.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault()) ?: "",
							minTemperature = futureForecastSummery.lowestMinTemperature?.roundToInt(),
							maxTemperature = futureForecastSummery.highestMaxTemperature?.roundToInt(),
							weatherType = futureForecastSummery.averageWeatherInfo
					))
				}
		return futureForecastsUiModel
	}

	private suspend fun fetchAndUpdateLocation() {
		currentLocationRelay.accept(UiState.Loading(true))

		phoneLocationManager.getLastAndCurrentAddress()
				.onStart { currentLocationRelay.accept(UiState.Loading(false)) }
				.distinctUntilChanged { lastLocation, currentLocation ->
					// filter addresses with the same city
					if (lastLocation is DataResult<AndroidLocation> && currentLocation is DataResult<AndroidLocation>) {
						lastLocation.data.locality == currentLocation.data.locality
					} else {
						false
					}
				}.onEach { locationResponse -> // create Location model from address & city
					when (locationResponse) {
						is DataResult -> {
							when (val cityResponse = cityRepository.perform(GetCity(), GetCity.Params(locationResponse.data.locality))) {
								is DataResult -> {
									val location = mapAddressAndCityToLocation(locationResponse.data, cityResponse.data)
									when (val user = userRepository.perform(UserLocalActions.GetUser())) {
										is DataResult -> {
											if (user.data.currentLocation != location) {
												user.data.currentLocation = location
												persistUser(user.data)
											}
										}
										else -> {} // todo handle errors
									}
								}
								else -> {} // todo handle errors
							}
						}
						else -> {} // todo handle errors
					}
				}
				.launchIn(viewModelScope)
	}

	private suspend fun persistUser(user: User) {
		// @TODO: 03/11/2020 do somethings with the return value
		userRepository.perform(UpdateUser(), UpdateUser.Params(user))
	}

	private fun createCurrentForecastUiModel(currentForecastState: DataResult<Forecast>): CurrentForecastUiModel {
		return currentForecastState.data.run {

			val isDayTime = let2(city.sunriseCalendar, city.sunsetCalendar) { sunrise, sunset ->
					calendar.get(Calendar.HOUR_OF_DAY) > sunrise.get(Calendar.HOUR_OF_DAY) &&
					calendar.get(Calendar.HOUR_OF_DAY) < sunset.get(Calendar.HOUR_OF_DAY) &&
					calendar.get(Calendar.MINUTE) > sunrise.get(Calendar.MINUTE) &&
					calendar.get(Calendar.MINUTE) < sunset.get(Calendar.MINUTE)
			} ?: true

			CurrentForecastUiModel(
					averageTemp = temperature.averageTemp.roundToInt(),
					maxTemperature = temperature.maxTemp.roundToInt(),
					minTemperature = temperature.minTemp.roundToInt(),
					description = "${weatherInfo.subDescription.toCamelCase()} ${if (isDayTime) "Night" else ""}",
					weatherType = weatherInfo.type,
					isDayTime = isDayTime
			)
		}
	}

	private fun createLocationUiModel(location: Location): CurrentLocationUiModel {
		return CurrentLocationUiModel(
				cityName = location.city.cityName,
				countryName = location.country
		)
	}
}