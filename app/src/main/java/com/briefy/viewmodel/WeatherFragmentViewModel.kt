package com.briefy.viewmodel

import androidx.lifecycle.viewModelScope
import com.briefy.model.*
import com.briefy.view.weather.models.HourlyForecastCompareUiModel
import com.briefy.usecases.local.UserLocalActions
import com.briefy.utils.*
import java.util.*
import kotlin.math.roundToInt
import com.briefy.model.WeatherInfo.*
import com.briefy.model.ActionResult.Empty as EmptyResult
import com.briefy.model.ActionResult.Data as DataResult
import com.briefy.model.ActionResult.Error as ErrorResult
import com.briefy.repository.ForecastRepository
import com.briefy.repository.Repository
import com.briefy.usecases.local.GetForecastsBetweenTimestamps
import com.briefy.usecases.remote.GetDayForecast
import com.briefy.view.weather.WeatherFragmentUiActions
import com.briefy.view.weather.WeatherFragmentUiStates
import com.briefy.view.weather.WeatherFragmentUiStates.*
import com.briefy.viewmodel.base.UiStateManager
import com.briefy.viewmodel.base.UiStateViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import kotlin.math.abs

private const val NUMBER_OF_HOURLY_FORECASTS = 6

class WeatherFragmentViewModel : UiStateViewModel<WeatherFragmentUiStates, WeatherFragmentUiActions>(), KoinComponent {

	private val userRepository: Repository by inject()
	private val forecastRepository: ForecastRepository by inject()

	override val uiStateManager: UiStateManager<WeatherFragmentUiStates> = UiStateManager(
			Main::class.java to Main()
	)

	fun viewReady() {
		viewModelScope.launch {
			userRepository.perform(UserLocalActions.ObserveUser())
					.onEach { userResponse ->
						when (userResponse) {
							is DataResult -> userResponse.data.currentLocation?.let {
								uiStateManager.set<Main> { copy(forecastLocation = UiState.Data(it.city.cityName)) }
								prepareForecastCompare(it.city)
							}
							is EmptyResult -> {} //TODO: show empty view
							is ErrorResult -> uiActionObservable.accept(WeatherFragmentUiActions.ShowErrorMessage(userResponse.info))
						}
					}
					.launchIn(viewModelScope)
		}
    }

    private suspend fun prepareForecastCompare(city: City) {
		uiStateManager.set<Main> { copy(forecastsComparison = UiState.Loading(true)) }

        val currTimestamp = Calendar.getInstance().timeInMillis
        val yesterdayTimestamp = Calendar.getInstance().apply {
			add(Calendar.DATE, -1)
			add(Calendar.HOUR, -3)
		}.timeInMillis

		val untilNowForecasts = forecastRepository.perform(GetForecastsBetweenTimestamps(), GetForecastsBetweenTimestamps.Params(city.cityId, yesterdayTimestamp, currTimestamp))
		val fromNowForecasts = forecastRepository.perform(GetDayForecast(), GetDayForecast.Params(city.cityName, NUMBER_OF_HOURLY_FORECASTS))

		val forecastsPairs = pairTodayAndYesterdayForecasts(untilNowForecasts, fromNowForecasts)
		val compareSummery = createCompareSummery(forecastsPairs)
		val compareList = createCompareList(forecastsPairs)

		uiStateManager
				.set<Main> { copy(forecastsComparison = UiState.Loading(false)) }
				.set<Main> { copy(forecastSummery = compareSummery, forecastsComparison = compareList) }
    }

	private fun createCompareSummery(forecastsPairs: List<Pair<Forecast, Forecast?>>): UiState<String> {
		val yesterdaySummery = FullDayForecastSummery()
		val todaySummery = FullDayForecastSummery()

		forecastsPairs.forEach { (todayForecast, yesterdayForecast) ->
			yesterdayForecast?.let {
				yesterdaySummery.addTemperature(it.temperature.averageTemp)
				yesterdaySummery.addWeatherInfoType(it.weatherInfo.type)
			}
			todaySummery.addTemperature(todayForecast.temperature.averageTemp)
			todaySummery.addWeatherInfoType(todayForecast.weatherInfo.type)
		}

		when {
			yesterdaySummery.forecastsCount == 0 -> return UiState.Data("Not enough weather information collected yet, Check again tomorrow")
			todaySummery.forecastsCount == 0 -> return UiState.Data("Not enough weather information available, Check again later")
			todaySummery.forecastsCount == 0 && yesterdaySummery.forecastsCount == 0 -> return UiState.Error("error in untilNowForecastsResult or fromNowForecastsResult")
		}

		// @TODO: 18/11/2020 re-think this
		let4(yesterdaySummery.averageTemperature?.roundToInt(), yesterdaySummery.averageWeatherInfo,
				todaySummery.averageTemperature?.roundToInt(), todaySummery.averageWeatherInfo
		) { yesterdayTemperature, yesterdayWeatherInfo, todayTemperature, todayWeatherInfo ->

			val quantityInfo = abs(todayTemperature - yesterdayTemperature).let {
				when {
					it >= 3 -> "a lot"
					it >= 1 -> "a bit"
					else -> ""
				}
			}

			val feelsLikePart = when {
				todayTemperature > yesterdayTemperature -> "$quantityInfo warmer than"
				todayTemperature < yesterdayTemperature -> "$quantityInfo colder than"
				else -> "about the same as"
			}

			val precipitationPart = if (todayWeatherInfo != yesterdayWeatherInfo) {
				val sentenceSeparateWord = if (quantityInfo.isEmpty()) "but" else "and"
				val weatherInfoWord = when (todayWeatherInfo) {
					Type.Clouds -> "${todayWeatherInfo.name.toLowerCase().removeSuffix("s")}y"
					Type.Windy -> todayWeatherInfo.name.toLowerCase()
					Type.Clear, Type.Rain, Type.Snow, Type.Thunder -> "${todayWeatherInfo.name.toLowerCase()}ing"
					Type.Unknown -> ""
				}
				"$sentenceSeparateWord it's $weatherInfoWord"
			} else ""

			val summery = "It feels $feelsLikePart yesterday $precipitationPart".trimEnd()
			return UiState.Data(summery)
		} ?: let {
			// @TODO: 18/11/2020 well reach here even when the yesterday is empty, and its not an error, maybe copy the messages from before the let4 here aswell
			return UiState.Error("something went wrong with creating compare list")
		}
	}

	private fun pairTodayAndYesterdayForecasts(untilNowForecastsResult: ActionResult<List<Forecast>>, fromNowForecastsResult: ActionResult<List<Forecast>>): List<Pair<Forecast, Forecast?>> {
		val untilNowForecasts = when (untilNowForecastsResult) {
			is DataResult -> untilNowForecastsResult.data
			else -> listOf()
		}

		val fromNowForecast = when (fromNowForecastsResult) {
			is DataResult -> fromNowForecastsResult.data
			else -> listOf()
		}

		val forecasts = untilNowForecasts + fromNowForecast
		val (yesterdayForecasts, todayForecasts) = forecasts.partition {
			it.calendar.timeInMillis < untilNowForecasts.lastOrNull()?.calendar?.timeInMillis ?: 0 // if no past forecasts found, all forecasts will be in todayForecasts
		}

		return todayForecasts.map { todayForecast ->
			val yesterdayForecast = yesterdayForecasts
					.find { it.calendar.apply { add(Calendar.HOUR, 24) }.timeInMillis == todayForecast.calendar.timeInMillis }
			Pair(todayForecast, yesterdayForecast)
		}
	}

	private fun createCompareList(forecastsPairs: List<Pair<Forecast, Forecast?>>): UiState<List<HourlyForecastCompareUiModel>> {
		val result = mutableListOf<HourlyForecastCompareUiModel>()

		val currentTimeIndex = if (forecastsPairs.size == NUMBER_OF_HOURLY_FORECASTS) 0 else 1
		forecastsPairs.forEachIndexed { index, (todayForecast, yesterdayForecast) ->
			result.add(HourlyForecastCompareUiModel(
					timestamp = todayForecast.calendar.timeInMillis,
					isCurrentTime = currentTimeIndex == index,
					yesterdayForecast = yesterdayForecast?.let {
						HourlyForecastCompareUiModel.DailyCompareUiModel(
								averageTemperature = it.temperature.averageTemp.roundToInt(),
								weatherType = it.weatherInfo.type
						)
					},
					todayForecast = todayForecast.let {
						HourlyForecastCompareUiModel.DailyCompareUiModel(
								averageTemperature = it.temperature.averageTemp.roundToInt(),
								weatherType = it.weatherInfo.type
						)
					}
			))
		}

		return UiState.Data(result.toList())
	}
}