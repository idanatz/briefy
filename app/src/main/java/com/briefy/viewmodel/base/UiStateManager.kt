package com.briefy.viewmodel.base

import com.briefy.utils.prettyToString
import com.jakewharton.rxrelay2.BehaviorRelay
import timber.log.Timber

class UiStateManager<UiState : Any>(
		vararg uiStates: Pair<Class<out UiState>, UiState>
) {

	private val cache = mutableMapOf<Class<out UiState>, UiState>()
	val observable: BehaviorRelay<UiState> = BehaviorRelay.create()

	init {
		cache.putAll(uiStates.toMutableSet())
	}

	inline fun <reified S : UiState> set(noinline block: (S.() -> S)? = null): UiStateManager<UiState> {
		val cachedValue = get(S::class.java)
		val newValue = block?.invoke(cachedValue) ?: cachedValue
		set(newValue)
		return this
	}

	fun <S : UiState> get(key: Class<out S>): S {
		return cache[key] as S
	}

	fun set(value: UiState) {
		cache[value::class.java] = value
		observable.accept(value).also {
			Timber.i("${value::class.java.superclass.simpleName} -> ${value.prettyToString()}")
		}
	}
}