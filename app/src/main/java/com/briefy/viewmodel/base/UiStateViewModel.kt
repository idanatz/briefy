package com.briefy.viewmodel.base

import androidx.lifecycle.ViewModel
import com.jakewharton.rxrelay2.PublishRelay

abstract class UiStateViewModel<UiState : Any, UiAction : Any> : ViewModel() {

	protected abstract val uiStateManager: UiStateManager<UiState>

	val uiStateObservable
		get() = uiStateManager.observable

	val uiActionObservable: PublishRelay<UiAction> = PublishRelay.create()
}