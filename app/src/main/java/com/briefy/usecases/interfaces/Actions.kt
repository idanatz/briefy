package com.briefy.usecases.interfaces

import kotlinx.coroutines.flow.Flow

interface Action<in In> {
	suspend fun execute(params: In?)
}

interface ValuableAction<in In, Intermediate, Out> {

	val mapper: Mapper<Intermediate?, Out>?
		get() = null

	suspend fun execute(params: In?): Intermediate?
}

interface ContinuesValuableAction<in In, Intermediate, Out>  {

	val mapper: Mapper<Intermediate?, Out>?
		get() = null

	suspend fun execute(params: In?): Flow<Intermediate?>
}