package com.briefy.usecases.interfaces

interface Mapper<In, Out> {

	fun map(value: In): Out
}