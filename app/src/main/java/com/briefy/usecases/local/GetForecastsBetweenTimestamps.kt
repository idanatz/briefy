package com.briefy.usecases.local

import androidx.room.Dao
import androidx.room.Query
import com.briefy.model.Forecast
import com.briefy.repository.need_to_sort.RoomClient
import com.briefy.usecases.interfaces.ValuableAction
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class GetForecastsBetweenTimestamps : ValuableAction<GetForecastsBetweenTimestamps.Params, List<Forecast>, List<Forecast>>, KoinComponent {

	private val room: RoomClient by inject()

	override suspend fun execute(params: Params?): List<Forecast>? {
		return params?.run {
			room.getForecastsBetweenTimestampsDao().execute(cityId, startTimestamp, endTimestamp)
		}
	}

	data class Params(
			val cityId: Int,
			val startTimestamp: Long,
			val endTimestamp: Long
	)

	@Dao
	interface RoomDao {
		@Query("SELECT * FROM forecast " +
				"WHERE cityId = :cityId " +
				"AND calendar >= :fromTimestamp " +
				"AND calendar <= :endTimestamp " +
				"ORDER BY calendar ASC ")
		suspend fun execute(cityId: Int, fromTimestamp: Long, endTimestamp: Long): List<Forecast>
	}
}