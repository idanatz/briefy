package com.briefy.usecases.local

import androidx.room.*
import androidx.room.OnConflictStrategy.ABORT
import com.briefy.model.User
import com.briefy.repository.need_to_sort.RoomClient
import com.briefy.usecases.interfaces.*
import kotlinx.coroutines.flow.Flow
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class UserLocalActions {

	class GetUser : ValuableAction<Unit, User, User>, KoinComponent {

		private val room: RoomClient by inject()

		override suspend fun execute(params: Unit?): User? {
			return room.getUserDao().execute()
		}

		@Dao
		interface RoomDao {
			@Query("SELECT * FROM user " +
					"LIMIT 1")
			suspend fun execute(): User?
		}
	}

	class ObserveUser : ContinuesValuableAction<Unit, User, User>, KoinComponent {

		private val room: RoomClient by inject()

		override suspend fun execute(params: Unit?): Flow<User?> {
			return room.observeUserDao().execute()
		}

		@Dao
		interface RoomDao {
			@Query("SELECT * FROM user " +
					"LIMIT 1")
			fun execute(): Flow<User?>
		}
	}
}