package com.briefy.usecases.local

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import com.briefy.model.Forecast
import com.briefy.repository.need_to_sort.RoomClient
import java.util.concurrent.TimeUnit
import com.briefy.usecases.interfaces.*
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class ForecastLocalActions {

	class InsertForecasts(private val forecasts: List<Forecast>) : Action<Unit>, KoinComponent {

		private val room: RoomClient by inject()

		override suspend fun execute(params: Unit?) {
			room.insertForecastsDao().execute(forecasts)
		}

		@Dao
		interface RoomDao {
			@Insert(onConflict = REPLACE)
			fun execute(forecasts: List<Forecast>)
		}
	}

	class GetOldForecasts : ValuableAction<Unit, List<Forecast>, List<Forecast>> , KoinComponent {

		private val room: RoomClient by inject()

		override suspend fun execute(params: Unit?): List<Forecast> {
			return room.getOldForecastsDao().execute(System.currentTimeMillis() - TimeUnit.HOURS.toMillis(48)) // 2 days back
		}

		@Dao
		interface RoomDao {
			@Query("SELECT * FROM forecast " + "WHERE calendar <= :fromTimestamp")
			suspend fun execute(fromTimestamp: Long): List<Forecast>
		}
	}
}