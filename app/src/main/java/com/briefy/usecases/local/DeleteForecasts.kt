package com.briefy.usecases.local

import androidx.room.Dao
import androidx.room.Delete
import com.briefy.model.Forecast
import com.briefy.repository.need_to_sort.RoomClient
import com.briefy.usecases.interfaces.Action
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class DeleteForecasts : Action<DeleteForecasts.Params>, KoinComponent {

	private val room: RoomClient by inject()

	override suspend fun execute(params: Params?) {
		params?.run {
			room.deleteForecastsDao().execute(forecasts)
		}
	}

	data class Params(val forecasts: List<Forecast>)

	@Dao
	interface RoomDao {
		@Delete
		suspend fun execute(forecasts: List<Forecast>)
	}
}