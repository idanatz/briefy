package com.briefy.usecases.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import com.briefy.model.User
import com.briefy.repository.need_to_sort.RoomClient
import com.briefy.usecases.interfaces.Action
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class InsertUser : Action<InsertUser.Params>, KoinComponent {

	private val room: RoomClient by inject()

	override suspend fun execute(params: Params?) {
		params?.run {
			room.insertUserDao().execute(user)
		}
	}

	data class Params(val user: User)

	@Dao
	interface RoomDao {
		@Insert(onConflict = OnConflictStrategy.ABORT)
		suspend fun execute(user: User)
	}
}