package com.briefy.usecases.local

import androidx.room.Dao
import androidx.room.Update
import com.briefy.model.User
import com.briefy.repository.need_to_sort.RoomClient
import com.briefy.usecases.interfaces.Action
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class UpdateUser : Action<UpdateUser.Params>, KoinComponent {

	private val room: RoomClient by inject()

	override suspend fun execute(params: Params?) {
		params?.run {
			room.updateUserDao().execute(user)
		}
	}

	data class Params(val user: User)

	@Dao
	interface RoomDao {
		@Update
		suspend fun execute(user: User)
	}
}