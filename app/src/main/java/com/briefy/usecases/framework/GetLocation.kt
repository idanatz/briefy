package com.briefy.usecases.framework

import com.briefy.location.CoGeocoder
import com.briefy.model.AndroidLocation
import com.briefy.usecases.interfaces.ValuableAction
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class GetLocation: ValuableAction<GetLocation.Params, List<AndroidLocation>, List<AndroidLocation>>, KoinComponent {

	private val geocoder: CoGeocoder by inject()

	override suspend fun execute(params: Params?): List<AndroidLocation>? {
		return params?.run {
			geocoder.getAddressListFromLocationName(locationName = locationName, maxResults = 30)
		}
	}

	data class Params(val locationName: String)
}