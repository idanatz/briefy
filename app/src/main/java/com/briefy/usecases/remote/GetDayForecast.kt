package com.briefy.usecases.remote

import com.briefy.mappers.ForecastDtoToCityMapper
import com.briefy.mappers.ForecastDtoToForecastsMapper
import com.briefy.model.Forecast
import com.briefy.model.open_weather.ForecastDto
import com.briefy.network.RetrofitClient
import com.briefy.usecases.interfaces.Mapper
import com.briefy.usecases.interfaces.ValuableAction
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import retrofit2.http.GET
import retrofit2.http.Query

class GetDayForecast: ValuableAction<GetDayForecast.Params, ForecastDto, List<Forecast>>, KoinComponent {

	private val retrofit: RetrofitClient by inject()
	override val mapper: Mapper<ForecastDto?, List<Forecast>> = ForecastDtoToForecastsMapper(ForecastDtoToCityMapper())

	override suspend fun execute(params: Params?): ForecastDto? {
		return params?.run {
			retrofit.create(RetrofitImpl::class.java).execute(cityName, numOfResults)
		}
	}

	data class Params(val cityName: String, val numOfResults: Int)

	interface RetrofitImpl {
		@GET("forecast")
		suspend fun execute(@Query(value = "q") cityName: String, @Query(value = "cnt") numOfResults: Int): ForecastDto
	}
}