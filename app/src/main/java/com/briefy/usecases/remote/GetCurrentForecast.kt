package com.briefy.usecases.remote

import com.briefy.mappers.WeatherDtoToCityMapper
import com.briefy.mappers.OWWeatherToForecastMapper
import com.briefy.model.Forecast
import com.briefy.model.open_weather.WeatherDto
import com.briefy.network.RetrofitClient
import com.briefy.usecases.interfaces.Mapper
import com.briefy.usecases.interfaces.ValuableAction
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import retrofit2.http.GET
import retrofit2.http.Query

class GetCurrentForecast: ValuableAction<GetCurrentForecast.Params, WeatherDto, Forecast>, KoinComponent {

	private val retrofit: RetrofitClient by inject()
	override val mapper: Mapper<WeatherDto?, Forecast> = OWWeatherToForecastMapper(WeatherDtoToCityMapper())

	override suspend fun execute(params: Params?): WeatherDto? {
		return params?.run {
			retrofit.create(RetrofitImpl::class.java).execute(cityName)
		}
	}

	data class Params(val cityName: String)

	interface RetrofitImpl {
		@GET("weather")
		suspend fun execute(@Query(value = "q") cityName: String): WeatherDto
	}
}