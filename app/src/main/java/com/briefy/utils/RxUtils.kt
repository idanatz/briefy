package com.briefy.utils

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.*
import java.util.concurrent.TimeUnit
import android.text.Editable
import android.text.TextWatcher
import io.reactivex.subjects.PublishSubject
import android.widget.AutoCompleteTextView

/**
 * Created by Idan Atsmon on 16/01/2018.
 */

// Single
fun <T> Single<T>.observeOnMain(): Single<T> = observeOn(AndroidSchedulers.mainThread())

// Observable
fun <T> Observable<T>.subscribeOnIO(): Observable<T> = subscribeOn(Schedulers.io())
fun <T> Observable<T>.observeOnMain(): Observable<T> = observeOn(AndroidSchedulers.mainThread())

// Disposable Extensions
fun Disposable.disposeBy(compositeDisposable: CompositeDisposable) = compositeDisposable.add(this)