package com.briefy.utils.ui

import android.animation.Animator
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.view.View
import androidx.core.view.postDelayed

inline fun View.animateAlpha(from: Float, to: Float, body: ObjectAnimator.() -> Unit): ObjectAnimator {
    val animator = ObjectAnimator.ofFloat(this, "alpha",from, to).apply {
        if (from == 0f) {
            addListener(animationStart = { visibility = View.VISIBLE })
        }

        if (to == 0f) {
            addListener(animationEnd = { visibility = View.INVISIBLE })
        }
    }
    body(animator)
    animator.start()
    return animator
}

inline fun View.animateShrinkHeight(body: ValueAnimator.() -> Unit = {}): ValueAnimator {
    val animator = ObjectAnimator.ofInt(height, 0).apply {
        addUpdateListener { valueAnimator ->
            val currValue = valueAnimator.animatedValue as Int

            if (currValue == 0) {
                visibility = View.GONE
            }

            setHeight(currValue)
        }
    }
    body(animator)
    animator.start()
    return animator
}

inline fun View.animateExpandHeight(height: Int, body: ValueAnimator.() -> Unit = {}): ValueAnimator {
    val animator = ObjectAnimator.ofInt(0, height).apply {
        addUpdateListener { valueAnimator ->
            val currValue = valueAnimator.animatedValue as Int

            if (currValue == 0) {
                postDelayed(100) {
                    visibility = View.VISIBLE
                }
            }

            setHeight(currValue)
        }
    }
    body(animator)
    animator.start()
    return animator
}

inline fun View.animateInt(from: Int, to: Int, body: ValueAnimator.() -> Unit): ValueAnimator {
    val animator = ObjectAnimator.ofInt(from, to)
    body(animator)
    animator.start()
    return animator
}

inline fun ValueAnimator.addListener(
        crossinline animationStart: (Animator) -> Unit = {},
        crossinline animationRepeat: (Animator) -> Unit = {},
        crossinline animationCancel: (Animator) -> Unit = {},
        crossinline animationEnd: (Animator) -> Unit = {}) {

    addListener(object : Animator.AnimatorListener {
        override fun onAnimationRepeat(animator: Animator) = animationRepeat(animator)
        override fun onAnimationEnd(animator: Animator) = animationEnd(animator)
        override fun onAnimationCancel(animator: Animator) = animationCancel(animator)
        override fun onAnimationStart(animator: Animator) = animationStart(animator)
    })
}