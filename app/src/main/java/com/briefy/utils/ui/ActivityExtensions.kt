package com.briefy.utils.ui

import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import android.view.WindowManager
import androidx.annotation.IdRes
import androidx.core.app.ComponentActivity
import androidx.viewbinding.ViewBinding
import com.briefy.utils.ui.binding.ActivityViewBindingProperty

fun AppCompatActivity.setStatusBarColor(color: Int) {
    window.run {
        addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        statusBarColor = color
    }
}

inline fun <A : ComponentActivity, T : ViewBinding> AppCompatActivity.viewBinding(
		crossinline bindingMethod: (LayoutInflater) -> T
) = ActivityViewBindingProperty<A, T> { bindingMethod(layoutInflater) }