package com.briefy.utils.ui

import android.content.Context
import android.graphics.Rect
import android.view.View
import android.util.DisplayMetrics
import android.view.ViewGroup

fun Boolean.toVisibility() = if(this) View.VISIBLE else View.GONE

fun Float.toDp(context: Context): Float {
    val resources = context.resources
    val metrics = resources.displayMetrics
    return this / (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
}

fun Float.toPx(context: Context): Float {
    val resources = context.resources
    val metrics = resources.displayMetrics
    return this * (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
}

fun View.getVisibleRect(): Rect {
    val rect = Rect()
    getGlobalVisibleRect(rect)
    return rect
}

fun ViewGroup.replacedViewsWith(views: List<View>) {
	removeAllViews()
	views.forEach { addView(it) }
}

fun View.setHeight(value: Int) {
    val lp = layoutParams
    lp?.let {
        lp.height = value
        layoutParams = lp
    }
}

fun View.setWidth(value: Int) {
    val lp = layoutParams
    lp?.let {
        lp.width = value
        layoutParams = lp
    }
}