package com.briefy.utils.ui.binding

import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding

class FragmentViewBindingProperty<F : Fragment, T : ViewBinding>(
		viewBinder: (F) -> T
) : ViewBindingProperty<F, T>(viewBinder) {

	override fun getLifecycleOwner(thisRef: F) = thisRef.viewLifecycleOwner
}