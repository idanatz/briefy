package com.briefy.utils.ui

import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.StateListDrawable
import androidx.annotation.ColorInt
import com.google.android.material.navigation.NavigationView

fun NavigationView.setItemTextColors(
		@ColorInt unselectedColor: Int,
		@ColorInt selectedColor: Int
) {
	val state = arrayOf(intArrayOf(-android.R.attr.state_checked), intArrayOf(android.R.attr.state_checked))
	val color = intArrayOf(unselectedColor, selectedColor)
	itemTextColor = ColorStateList(state, color)
}

fun NavigationView.setItemBackgroundColor(
		@ColorInt unselectedColor: Int = Color.TRANSPARENT,
		@ColorInt selectedColor: Int
) {
	itemBackground = StateListDrawable().apply {
		addState(intArrayOf(-android.R.attr.state_checked), ColorDrawable(unselectedColor))
		addState(intArrayOf(android.R.attr.state_checked), ColorDrawable(selectedColor))
	}
}