package com.briefy.utils.ui

import android.widget.EditText
import androidx.core.widget.doOnTextChanged
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

val EditText.textChangesFlow: Flow<CharSequence>
	get() = callbackFlow {
		val textWatcher = doOnTextChanged { text, _, _, _ ->
			if (text != null) {
				offer(text)
			}
		}
		awaitClose {
			removeTextChangedListener(textWatcher)
		}
	}