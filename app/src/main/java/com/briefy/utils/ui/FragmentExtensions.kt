package com.briefy.utils.ui

import android.view.LayoutInflater
import android.view.View
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.briefy.utils.ui.binding.FragmentViewBindingProperty

inline fun <F : Fragment, T : ViewBinding> Fragment.viewBinding(
		crossinline bindingMethod: (View) -> T
) = FragmentViewBindingProperty<F, T> { bindingMethod(requireView()) }