package com.briefy.utils.ui.binding

import androidx.core.app.ComponentActivity
import androidx.viewbinding.ViewBinding

class ActivityViewBindingProperty<A : ComponentActivity, T : ViewBinding>(
		viewBinder: (A) -> T
) : ViewBindingProperty<A, T>(viewBinder) {

	override fun getLifecycleOwner(thisRef: A) = thisRef
}