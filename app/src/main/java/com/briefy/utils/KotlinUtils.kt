package com.briefy.utils

/**
 * Created by Idan Atsmon on 30/04/2018.
 */
inline fun <A: Any?, B: Any?, R> let2(v1: A?, v2: B?, callback: (A, B) -> R) : R? {
    return v1?.let { v1Verified ->
        v2?.let { v2Verified ->
            callback(v1Verified, v2Verified)
        }
    }
}

inline fun <A: Any?, B: Any?, C: Any?, R> let3(v1: A?, v2: B?, v3: C?, callback: (A, B, C) -> R) : R? {
    return v1?.let { v1Verified ->
        v2?.let { v2Verified ->
            v3?.let { v3Verified ->
                callback(v1Verified, v2Verified, v3Verified)
            }
        }
    }
}

inline fun <A: Any?, B: Any?, C: Any?, D: Any?, R> let4(v1: A?, v2: B?, v3: C?, v4: D?, callback: (A, B, C, D) -> R) : R? {
    return v1?.let { v1Verified ->
        v2?.let { v2Verified ->
            v3?.let { v3Verified ->
                v4?.let { v4Verified ->
                    callback(v1Verified, v2Verified, v3Verified, v4Verified)
                }
            }
        }
    }
}

inline fun <A: Any?, B: Any?, C: Any?, D: Any?, E: Any?, R> let5(v1: A?, v2: B?, v3: C?, v4: D?, v5: E?, callback: (A, B, C, D, E) -> R) : R? {
    return v1?.let { v1Verified ->
        v2?.let { v2Verified ->
            v3?.let { v3Verified ->
                v4?.let { v4Verified ->
                    v5?.let { v5Verified ->
                        callback(v1Verified, v2Verified, v3Verified, v4Verified, v5Verified)
                    }
                }
            }
        }
    }
}