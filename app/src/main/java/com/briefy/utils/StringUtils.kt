package com.briefy.utils

/**
 * Created by Idan Atsmon on 07/05/2018.
 */

fun String.toCamelCase() = split(" ").joinToString(separator = " ") { word -> Character.toTitleCase(word[0]) + word.substring(1) }