package com.briefy.utils

/**
 * Created by Idan Atsmon on 20/05/2018.
 */

fun Int.percent(percent: Float) = this * percent / 100