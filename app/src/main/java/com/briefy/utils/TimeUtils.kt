package com.briefy.utils

import java.text.SimpleDateFormat
import java.util.*

fun Calendar.setToTimestamp(timestampInMillis: Long): Calendar {
	timeInMillis = timestampInMillis
	return this
}

fun Calendar.setToTodayMidnight(): Calendar {
	set(Calendar.HOUR_OF_DAY, 0)
	set(Calendar.MINUTE, 0)
	set(Calendar.SECOND, 0)
	set(Calendar.MILLISECOND, 0)
	return this
}

fun Calendar.setToTodayAt(hourOfDay: Int): Calendar = apply {
	add(Calendar.DATE, 0)
	set(Calendar.HOUR_OF_DAY, hourOfDay)
	set(Calendar.MINUTE, 0)
	set(Calendar.SECOND, 0)
	set(Calendar.MILLISECOND, 0)
}

fun Calendar.setToTomorrowAt(hourOfDay: Int): Calendar = apply {
	add(Calendar.DATE, 1)
	set(Calendar.HOUR_OF_DAY, hourOfDay)
	set(Calendar.MINUTE, 0)
	set(Calendar.SECOND, 0)
	set(Calendar.MILLISECOND, 0)
}

fun Calendar.setToYesterdayMidnight(): Calendar = setToTodayMidnight().apply { add(Calendar.DATE, -1) }

fun Calendar.affectByDaylightSaving(): Calendar = apply { add(Calendar.HOUR, if (isDaylightSaving()) 0 else -1) }

/**
 * Extension method to get Date for String with specified format.
 */
fun Long.toDateInFormat(format: String): String {
	val dateFormat = SimpleDateFormat(format, Locale.getDefault())
	return dateFormat.format(this)
}

fun isDaylightSaving() = TimeZone.getDefault().inDaylightTime(Date())
