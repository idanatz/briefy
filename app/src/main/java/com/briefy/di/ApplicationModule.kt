@file:Suppress("RemoveExplicitTypeArguments")

package com.briefy.di

import androidx.core.app.NotificationManagerCompat
import androidx.work.WorkManager
import com.briefy.PhoneLocationManager
import com.briefy.PushNotificationManager
import com.briefy.ThemeEngine
import com.briefy.location.CoGeocoder
import com.briefy.location.CoLocation
import com.briefy.logging.AppTree
import org.koin.core.module.Module
import org.koin.dsl.module
import timber.log.Timber

val applicationModule : Module = module {

	// logging
	factory<Timber.Tree> { AppTree() }

	// notifications
	single<NotificationManagerCompat> { NotificationManagerCompat.from(get()) }
	factory { PhoneLocationManager(context = get(), locationProvider = get(), geocoderProvider = get()) }

	// works
	single<WorkManager> { WorkManager.getInstance(get()) }

	// location
	factory { CoLocation.from(get()) }
	factory { CoGeocoder.from(get()) }
	factory { PushNotificationManager(context = get(), notificationManager = get()) }


	// TODO: should be here?
	single { ThemeEngine(context = get()) }
}