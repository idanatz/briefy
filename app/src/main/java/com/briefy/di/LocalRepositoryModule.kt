package com.briefy.di

import com.briefy.repository.CityRepository
import com.briefy.repository.ForecastRepository
import com.briefy.repository.Repository
import org.koin.core.module.Module
import org.koin.dsl.module

val localRepositoryModule : Module = module {

	factory { Repository() }
	factory { ForecastRepository() }
	factory { CityRepository() }
}