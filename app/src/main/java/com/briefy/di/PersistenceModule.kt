package com.briefy.di

import androidx.room.Room
import com.briefy.repository.need_to_sort.RoomClient
import org.koin.core.module.Module
import org.koin.dsl.module

val persistenceModule : Module = module {

    single { Room.databaseBuilder(get(), RoomClient::class.java, "BriefyRoomDatabase").build() }
}