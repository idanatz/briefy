package com.briefy.repository

import com.briefy.model.ActionResult
import com.briefy.model.ActionResult.Data as DataResult
import com.briefy.model.ActionResult.Empty as EmptyResult
import com.briefy.model.ActionResult.Success as SuccessResult
import com.briefy.model.ActionResult.Error as ErrorResult
import com.briefy.usecases.interfaces.Action
import com.briefy.usecases.interfaces.ContinuesValuableAction
import com.briefy.usecases.interfaces.Mapper
import com.briefy.usecases.interfaces.ValuableAction
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

open class Repository (
		private val dispatcher: CoroutineDispatcher = Dispatchers.IO,
		private val actionErrorMapper: Mapper<Throwable?, ErrorResult?>? = null
) {
	suspend fun <In : Any?> perform(action: Action<In>, params: In? = null): ActionResult<Nothing> = withContext(dispatcher) {
		try {
			action.execute(params)
			SuccessResult
		} catch (throwable: Throwable) {
			when (throwable) {
				is CancellationException -> throw throwable
				else -> actionErrorMapper?.map(throwable) ?: ErrorResult.Unknown(throwable.message.toString())
			}
		}
	}

	suspend fun <In : Any?, Intermediate : Any, Out : Any> perform(action: ValuableAction<In, Intermediate, Out>, params: In? = null): ActionResult<Out> = withContext(dispatcher) {
		try {
			val value = action.execute(params)
			val mapped = action.mapper?.map(value) ?: value as Out?

			when {
				mapped == null -> EmptyResult
				mapped is Collection<*> && mapped.isEmpty() -> EmptyResult
				else -> DataResult(mapped)
			}
		} catch (throwable: Throwable) {
			when (throwable) {
				is CancellationException -> throw throwable
				else -> actionErrorMapper?.map(throwable) ?: ErrorResult.Unknown(throwable.message.toString())
			}
		}
	}

	suspend fun <In : Any?, Intermediate : Any?, Out : Any> perform(action: ContinuesValuableAction<In, Intermediate, Out>, params: In? = null): Flow<ActionResult<Out>> {
		return action.execute(params).map { action.mapper?.map(it) ?:  it as Out? }.distinctUntilChanged().map { value ->
			when {
				value == null -> EmptyResult
				value is Collection<*> && value.isEmpty() -> EmptyResult
				else -> DataResult(value)
			}
		}
		.catch { throwable ->
			when (throwable) {
				is CancellationException -> throw throwable
				else -> emit(actionErrorMapper?.map(throwable) ?: ErrorResult.Unknown(throwable.message.toString()))
			}
		}
		.flowOn(dispatcher)
	}
}