package com.briefy.repository

import com.briefy.mappers.OWExceptions
import com.briefy.model.ActionResult.Error as ErrorResult
import com.briefy.usecases.interfaces.Mapper
import retrofit2.HttpException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class ForecastErrorMapper : Mapper<Throwable?, ErrorResult?> {

	override fun map(value: Throwable?): ErrorResult? = when (value) {
		// mapper exceptions
		is OWExceptions.ForecastNotFoundException -> ErrorResult.DataNotFound(value.message ?: "")
		is OWExceptions.CityNotFoundException -> ErrorResult.DataNotFound(value.message ?: "")

		// network exceptions
		is HttpException -> {
			when (value.code()) {
				301 -> ErrorResult.Network.ResourceRemoved(value.message())
				302 -> ErrorResult.Network.RemovedResourceFound(value.message())
				403 -> ErrorResult.Network.ResourceForbidden(value.message())
				404 -> ErrorResult.Network.ResourceNotFound(value.message())
				429 -> ErrorResult.Network.TooManyRequests(value.message())
				500 -> ErrorResult.Network.InternalServerError(value.message())
				502 -> ErrorResult.Network.BadGateWay(value.message())
				else -> ErrorResult.Network.Unknown("cod: ${value.code()}, error: ${value.message()}")
			}
		}
		is UnknownHostException -> ErrorResult.Network.Unknown("UnknownHostException")
		is SocketTimeoutException -> ErrorResult.Network.Unknown("SocketTimeoutException")
		else -> null
	}
}