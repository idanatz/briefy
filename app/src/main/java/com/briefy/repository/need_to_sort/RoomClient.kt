package com.briefy.repository.need_to_sort

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.briefy.model.Forecast
import com.briefy.model.Location
import com.briefy.model.User
import com.briefy.model.WeatherInfo
import com.briefy.usecases.local.*
import com.briefy.utils.setToTimestamp
import java.util.*
import com.google.gson.reflect.TypeToken
import com.google.gson.Gson

@Database(entities = [Forecast::class, User::class], version = 1)
@TypeConverters(CalenderConverter::class, WeatherTypeConverter::class, LocationSetTypeConverter::class)
abstract class RoomClient : RoomDatabase() {

	abstract fun insertForecastsDao(): ForecastLocalActions.InsertForecasts.RoomDao
	abstract fun deleteForecastsDao(): DeleteForecasts.RoomDao
	abstract fun getForecastsBetweenTimestampsDao(): GetForecastsBetweenTimestamps.RoomDao
	abstract fun getOldForecastsDao(): ForecastLocalActions.GetOldForecasts.RoomDao

	abstract fun insertUserDao(): InsertUser.RoomDao
	abstract fun updateUserDao(): UpdateUser.RoomDao
	abstract fun getUserDao(): UserLocalActions.GetUser.RoomDao
	abstract fun observeUserDao(): UserLocalActions.ObserveUser.RoomDao
}

class CalenderConverter {

    @TypeConverter
    fun timestampToCalender(timestamp: Long): Calendar? = when (timestamp) {
        -1L -> null
        else -> Calendar.getInstance().setToTimestamp(timestamp)
    }

    @TypeConverter
    fun calenderToTimestamp(calendar: Calendar?): Long = calendar?.timeInMillis ?: -1L
}

class WeatherTypeConverter {

    @TypeConverter
    fun weatherTypeToString(weatherType: WeatherInfo.Type): String = weatherType.name

    @TypeConverter
    fun stringToWeatherType(name: String): WeatherInfo.Type = WeatherInfo.Type.valueOf(name)
}

class LocationSetTypeConverter {

    @TypeConverter
    fun locationSetTypeToString(locations: Set<Location>): String {
        val gson = Gson()
        val type = object : TypeToken<Set<Location>>() {}.type
        return gson.toJson(locations, type)
    }

    @TypeConverter
    fun stringToLocationSet(locations: String): Set<Location> {
        val gson = Gson()
        val type = object : TypeToken<Set<Location>>() {}.type
        return gson.fromJson(locations, type)
    }
}