package com.briefy.works

import android.content.Context
import androidx.work.*
import com.briefy.PhoneLocationManager
import com.briefy.PushNotificationManager
import com.briefy.mappers.LocationMapper.Companion.mapAddressAndCityToLocation
import com.briefy.model.ActionResult.Data as DataResult
import com.briefy.model.ActionResult.Success as SuccessResult
import com.briefy.model.ActionResult.Error as ErrorResult
import com.briefy.repository.CityRepository
import com.briefy.repository.ForecastRepository
import com.briefy.repository.Repository
import com.briefy.usecases.local.UserLocalActions
import com.briefy.usecases.local.ForecastLocalActions
import com.briefy.usecases.local.UpdateUser
import com.briefy.usecases.remote.GetCity
import com.briefy.usecases.remote.GetThreeDaysForecast
import java.io.PrintWriter
import java.io.StringWriter
import java.util.*
import java.util.concurrent.TimeUnit
import com.briefy.utils.setToTomorrowAt
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class GetForecastsWork(appContext: Context, workerParams: WorkerParameters) : CoroutineWorker(appContext, workerParams), KoinComponent {

    private val workManager: WorkManager by inject()
    private val pushNotificationManager: PushNotificationManager by inject()
	private val phoneLocationManager: PhoneLocationManager by inject()
	private val userRepository: Repository by inject()
	private val forecastRepository: ForecastRepository by inject()
	private val cityRepository: CityRepository by inject()

	companion object {
        val TAG: String = GetForecastsWork::class.java.simpleName

        fun create(): OneTimeWorkRequest {
            val timeDiff = Calendar.getInstance().setToTomorrowAt(2).timeInMillis - Calendar.getInstance().timeInMillis

            val constraints = Constraints.Builder()
                    .setRequiredNetworkType(NetworkType.CONNECTED)
                    .build()

            return OneTimeWorkRequestBuilder<GetForecastsWork>()
					.setInitialDelay(timeDiff, TimeUnit.MILLISECONDS)
                    .setConstraints(constraints)
                    .addTag(TAG)
                    .build()
        }
    }

	//TODO: inject dep
	//TODO: do something with this errors
	override suspend fun doWork(): Result {
		val workResult = try {
			when (val userResponse = userRepository.perform(UserLocalActions.GetUser())) {
				is DataResult -> {
					// Part 1 - get current user location and save it in user model
					when (val locationResponse = phoneLocationManager.getCurrentLocation()) { 	//TODO: why persisted current location? need to re-fetch the user location here!
						is DataResult -> {
							when (val cityResponse = cityRepository.perform(GetCity(), GetCity.Params(locationResponse.data.locality))) {
								is DataResult -> {
									val location = mapAddressAndCityToLocation(locationResponse.data, cityResponse.data)
									if (userResponse.data.currentLocation != location) {
										userResponse.data.currentLocation = location
										userRepository.perform(UpdateUser(), UpdateUser.Params(userResponse.data))
									}
								}
								is ErrorResult -> {
								}
							}
						}
						is ErrorResult -> {
						}
					}

					// Part 2 - for each user location fetch forecast
					val userLocations = userResponse.data.savedLocations + userResponse.data.currentLocation
					userLocations.filterNotNull().forEach { location ->
						when (val forecastResponse = forecastRepository.perform(GetThreeDaysForecast(), GetThreeDaysForecast.Params(location.city.cityName, true))) {
							is DataResult -> {
								when (val insertResponse = forecastRepository.perform(ForecastLocalActions.InsertForecasts(forecastResponse.data))) {
									is SuccessResult -> pushNotificationManager.sendNotification("$TAG - Finished", "Inserted ${forecastResponse.data.size} forecasts to ${location.city.cityName}")
									is ErrorResult -> throw Throwable("error in insert forecast, ${insertResponse.info}")
									else -> throw Throwable("unknown error in insertResponse")
								}
							}
							is ErrorResult -> throw Throwable("error in forecastResponse, ${forecastResponse.info}")
							else -> throw Throwable("unknown error in forecastResponse")
						}
					}
					Result.success()
				}
				is ErrorResult -> throw Throwable("error in userResponse, ${userResponse.info}")
				else -> throw Throwable("unknown error in userResponse")
			}
		} catch (throwable: Throwable) {
			val result = StringWriter()
			val printWriter = PrintWriter(result)
			throwable.printStackTrace(printWriter)
			val content = """
				reason: ${throwable.message}
				stack: $result
			""".trimIndent()
			pushNotificationManager.sendNotification("${DeleteOldForecastsWork.TAG} - Error", content)
			Result.failure()
		}

		workManager.enqueueUniqueWork(TAG, ExistingWorkPolicy.REPLACE, create())
		return workResult
	}
}