package com.briefy.works

import android.content.Context
import androidx.work.*
import com.briefy.PushNotificationManager
import com.briefy.model.ActionResult.Data as DataResult
import com.briefy.model.ActionResult.Empty as EmptyResult
import com.briefy.model.ActionResult.Success as SuccessResult
import com.briefy.model.ActionResult.Error as ErrorResult
import com.briefy.usecases.local.ForecastLocalActions
import com.briefy.utils.setToTomorrowAt
import com.briefy.repository.ForecastRepository
import com.briefy.usecases.local.DeleteForecasts
import kotlinx.coroutines.coroutineScope
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.io.PrintWriter
import java.io.StringWriter
import java.util.*
import java.util.concurrent.TimeUnit

class DeleteOldForecastsWork(appContext: Context, workerParams: WorkerParameters) : CoroutineWorker(appContext, workerParams), KoinComponent {

	private val workManager: WorkManager by inject()
	private val pushNotificationManager: PushNotificationManager by inject()
    private val forecastRepository: ForecastRepository by inject()

    companion object {
        val TAG: String = DeleteOldForecastsWork::class.java.simpleName

        fun create(): OneTimeWorkRequest {
            val timeDiff = Calendar.getInstance().setToTomorrowAt(2).timeInMillis - Calendar.getInstance().timeInMillis

            return OneTimeWorkRequestBuilder<DeleteOldForecastsWork>()
					.setInitialDelay(timeDiff, TimeUnit.MILLISECONDS)
                    .addTag(TAG)
                    .build()
        }
    }

	//TODO: do something with this errors
	override suspend fun doWork(): Result = coroutineScope {
		val workResult = try {
			when (val forecastResponse = forecastRepository.perform(ForecastLocalActions.GetOldForecasts())) {
				is DataResult -> {
					when (val deleteResponse = forecastRepository.perform(DeleteForecasts(), DeleteForecasts.Params(forecastResponse.data))) {
						is SuccessResult -> {
							pushNotificationManager.sendNotification("$TAG - Finished", "Deleted ${forecastResponse.data.size} forecasts")
							Result.success()
						}
						is ErrorResult -> throw Throwable("error in delete forecast, ${deleteResponse.info}")
						else -> throw Throwable("unknown error in deleteResponse")
					}
				}
				is EmptyResult -> Result.success()
				is ErrorResult -> throw Throwable("error in forecastResponse, ${forecastResponse.info}")
				else -> throw Throwable("unknown error in forecastResponse")
			}
		} catch (throwable: Throwable) {
			val result = StringWriter()
			val printWriter = PrintWriter(result)
			throwable.printStackTrace(printWriter)
			val content = """
				reason: ${throwable.message}
				stack: $result
			""".trimIndent()
			pushNotificationManager.sendNotification("$TAG - Error", content)
			Result.failure()
		}

		workManager.enqueueUniqueWork(TAG, ExistingWorkPolicy.REPLACE, create())
		workResult
	}
}