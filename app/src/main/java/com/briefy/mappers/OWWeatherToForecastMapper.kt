package com.briefy.mappers

import com.briefy.model.*
import com.briefy.model.open_weather.WeatherDto
import com.briefy.usecases.interfaces.Mapper
import com.briefy.utils.setToTimestamp
import java.lang.NullPointerException
import java.util.*

class OWWeatherToForecastMapper(
		private val cityMapper: WeatherDtoToCityMapper
) : Mapper<WeatherDto?, Forecast> {

	override fun map(value: WeatherDto?): Forecast {
		return when (value?.cod) {
			"200" -> mapOWWeatherToForecast(value)
			"404" -> throw OWExceptions.CityNotFoundException()
			null -> throw NullPointerException("OWWeather is null")
			else -> throw UnknownError("unknown error, code ${value.cod}")
		}
	}

	private fun mapOWWeatherToForecast(remoteWeather: WeatherDto): Forecast {
		if (remoteWeather.id == null) throw IllegalArgumentException("missing city id")
		if (remoteWeather.name == null)  throw IllegalArgumentException("missing city name")
		if (remoteWeather.dt == null) throw IllegalArgumentException("missing time")
		if (remoteWeather.main == null) {
			throw IllegalArgumentException("missing main temp")
		} else {
			if (remoteWeather.main.tempMax == null) throw IllegalArgumentException("missing max temp")
			if (remoteWeather.main.tempMin == null) throw IllegalArgumentException("missing min temp")
		}

		return Forecast(
				city = cityMapper.map(remoteWeather),
				weatherInfo = WeatherInfoMapper.mapOWWeatherToWeatherInfo(remoteWeather),
				calendar = Calendar.getInstance().setToTimestamp(remoteWeather.dt.toLong() * 1000),
				temperature = Temperature(
						maxTemp = remoteWeather.main.tempMax,
						minTemp = remoteWeather.main.tempMin),
				id = remoteWeather.id + remoteWeather.dt
		)
	}
}