package com.briefy.mappers

class OWExceptions {
	class CityNotFoundException : Throwable("")
	class ForecastNotFoundException : Throwable("")
}