package com.briefy.mappers

import com.briefy.model.City
import com.briefy.model.open_weather.WeatherDto
import com.briefy.usecases.interfaces.Mapper
import com.briefy.utils.setToTimestamp
import java.lang.NullPointerException
import java.util.*

class WeatherDtoToCityMapper : Mapper<WeatherDto?, City> {

	override fun map(value: WeatherDto?): City {
		return when (value?.cod) {
			"200" -> mapWeatherDtoToCity(value)
			"404" -> throw OWExceptions.ForecastNotFoundException()
			null -> throw NullPointerException("OWWeather is null")
			else -> throw UnknownError("unknown error, code ${value.cod}")
		}
	}

	private fun mapWeatherDtoToCity(weatherDto: WeatherDto): City {
		if (weatherDto.id == null) throw IllegalArgumentException("missing city id")
		if (weatherDto.name == null)  throw IllegalArgumentException("missing city name")
		if (weatherDto.sys == null) throw IllegalArgumentException("missing city sys information")
		if (weatherDto.sys.sunrise == null) throw IllegalArgumentException("missing city sunrise information")
		if (weatherDto.sys.sunset == null) throw IllegalArgumentException("missing city sunset information")

		return City(
				cityId = weatherDto.id,
				cityName = weatherDto.name,
				sunriseCalendar = Calendar.getInstance().setToTimestamp(weatherDto.sys.sunrise.toLong() * 1000),
				sunsetCalendar = Calendar.getInstance().setToTimestamp(weatherDto.sys.sunset.toLong() * 1000)
		)
	}
}