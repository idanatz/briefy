package com.briefy.mappers

import com.briefy.model.AndroidLocation
import com.briefy.model.City
import com.briefy.model.Location

/**
 * Created by Idan Atsmon on 17/04/2018.
 */

class LocationMapper {

    companion object {
        fun mapAddressAndCityToLocation(androidLocation: AndroidLocation, city: City): Location {
            if (androidLocation.countryName == null) throw IllegalArgumentException("missing country")

            return Location(
                    country = androidLocation.countryName,
                    city = city
            )
        }
    }
}