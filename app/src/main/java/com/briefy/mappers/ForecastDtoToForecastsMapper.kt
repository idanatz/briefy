package com.briefy.mappers

import com.briefy.model.Forecast
import com.briefy.model.open_weather.ForecastDto
import com.briefy.model.Temperature
import com.briefy.usecases.interfaces.Mapper
import com.briefy.utils.setToTimestamp
import java.lang.NullPointerException
import java.util.*

class ForecastDtoToForecastsMapper(
		private val cityMapper: ForecastDtoToCityMapper
) : Mapper<ForecastDto?, List<Forecast>> {

	override fun map(value: ForecastDto?): List<Forecast> {
		return when (value?.cod) {
			"200" -> mapForecastDtoToForecasts(value)
			"404" -> throw OWExceptions.ForecastNotFoundException()
			null -> throw NullPointerException("OWForecast is null")
			else -> throw UnknownError("unknown error, code ${value.cod}")
		}
	}

	private fun mapForecastDtoToForecasts(forecastDto: ForecastDto): List<Forecast> {
		val result = mutableListOf<Forecast>()

		if (forecastDto.city == null) {
			throw IllegalArgumentException("missing city information")
		} else {
			if (forecastDto.city.id == null) throw IllegalArgumentException("missing city id")
			if (forecastDto.city.name == null)  throw IllegalArgumentException("missing city name")
		}

		forecastDto.list?.forEach {

			if (it.dt == null) throw IllegalArgumentException("missing time")
			if (it.main == null) {
				throw IllegalArgumentException("missing main temp")
			} else {
				if (it.main.tempMax == null) throw IllegalArgumentException("missing max temp")
				if (it.main.tempMin == null) throw IllegalArgumentException("missing min temp")
			}

			result.add(Forecast(
					city = cityMapper.map(forecastDto),
					weatherInfo = WeatherInfoMapper.mapOWWeatherToWeatherInfo(it),
					calendar = Calendar.getInstance().setToTimestamp(it.dt.toLong() * 1000),
					temperature = Temperature(
							maxTemp = it.main.tempMax,
							minTemp = it.main.tempMin),
					id = forecastDto.city.id + it.dt
			))
		}
		return result
	}
}