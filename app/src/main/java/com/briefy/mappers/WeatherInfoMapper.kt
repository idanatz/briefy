package com.briefy.mappers

import com.briefy.model.WeatherInfo
import com.briefy.model.open_weather.WeatherDto

class WeatherInfoMapper {

    companion object {
        fun mapOWWeatherToWeatherInfo(remoteWeather: WeatherDto): WeatherInfo {
            if (remoteWeather.weather == null) {
                throw IllegalArgumentException("missing weather list")
            } else {
                remoteWeather.weather.firstOrNull().let {
                    if (it == null) {
                        throw IllegalArgumentException("missing weather description")
                    } else {
                        if (it.main == null) throw IllegalArgumentException("missing main description")
                        if (it.description == null) throw IllegalArgumentException("missing sub description")
                        if (it.id == null) throw IllegalArgumentException("missing weather id")

                        return WeatherInfo(
                                type = mapWeatherIdToWeatherInfoType(it.id),
                                mainDescription = it.main,
                                subDescription = it.description
                        )
                    }
                }
            }
        }

        private fun mapWeatherIdToWeatherInfoType(id: Int): WeatherInfo.Type {
            return when (id) {
                in 200..299 -> WeatherInfo.Type.Thunder
                in 300..399, in 500..599 -> WeatherInfo.Type.Rain
                in 600..699 -> WeatherInfo.Type.Snow
                701, 721, 741, in 801..809 -> WeatherInfo.Type.Clouds
                800 -> WeatherInfo.Type.Clear
                905, in 952..956 -> WeatherInfo.Type.Windy
                else -> WeatherInfo.Type.Unknown
            }
        }
    }
}