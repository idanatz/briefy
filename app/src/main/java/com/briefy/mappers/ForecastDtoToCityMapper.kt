package com.briefy.mappers

import com.briefy.model.City
import com.briefy.model.open_weather.ForecastDto
import com.briefy.usecases.interfaces.Mapper
import java.lang.NullPointerException

class ForecastDtoToCityMapper : Mapper<ForecastDto?, City> {

	override fun map(value: ForecastDto?): City{
		return when (value?.cod) {
			"200" -> mapForecastDtoToCity(value)
			"404" -> throw OWExceptions.ForecastNotFoundException()
			null -> throw NullPointerException("OWForecast is null")
			else -> throw UnknownError("unknown error, code ${value.cod}")
		}
	}

	private fun mapForecastDtoToCity(forecastDto: ForecastDto): City {
		if (forecastDto.city == null) {
			throw IllegalArgumentException("missing city information")
		} else {
			if (forecastDto.city.id == null) throw IllegalArgumentException("missing city id")
			if (forecastDto.city.name == null) throw IllegalArgumentException("missing city name")
		}

		return City(
				cityId = forecastDto.city.id,
				cityName = forecastDto.city.name
		)
	}

}